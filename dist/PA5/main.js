(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: routes, AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _main_main_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./main/main.component */ "./src/app/main/main.component.ts");
/* harmony import */ var _auth_auth_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./auth/auth.component */ "./src/app/auth/auth.component.ts");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/profile/profile.component.ts");
/* harmony import */ var _auth_login_login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./auth/login/login.component */ "./src/app/auth/login/login.component.ts");
/* harmony import */ var _auth_registeration_registeration_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./auth/registeration/registeration.component */ "./src/app/auth/registeration/registeration.component.ts");
/* harmony import */ var _profile_update_info_update_info_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./profile/update-info/update-info.component */ "./src/app/profile/update-info/update-info.component.ts");
/* harmony import */ var _profile_current_info_current_info_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./profile/current-info/current-info.component */ "./src/app/profile/current-info/current-info.component.ts");
/* harmony import */ var _main_following_following_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./main/following/following.component */ "./src/app/main/following/following.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var routes = [
    { path: '', redirectTo: '/auth', pathMatch: 'full' },
    { path: 'main', component: _main_main_component__WEBPACK_IMPORTED_MODULE_2__["MainComponent"] },
    { path: 'auth', component: _auth_auth_component__WEBPACK_IMPORTED_MODULE_3__["AuthComponent"] },
    { path: 'profile', component: _profile_profile_component__WEBPACK_IMPORTED_MODULE_4__["ProfileComponent"] },
    { path: 'login', component: _auth_login_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"] },
    { path: 'registration', component: _auth_registeration_registeration_component__WEBPACK_IMPORTED_MODULE_6__["RegisterationComponent"] },
    { path: 'updateInfo', component: _profile_update_info_update_info_component__WEBPACK_IMPORTED_MODULE_7__["UpdateInfoComponent"] },
    { path: 'currentInfo', component: _profile_current_info_current_info_component__WEBPACK_IMPORTED_MODULE_8__["CurrentInfoComponent"] },
    { path: 'following', component: _main_following_following_component__WEBPACK_IMPORTED_MODULE_9__["FollowingComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes, { useHash: true })],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "$grid-breakpoints: (\n  xs: 0,\n  sm: 576px,\n  md: 768px,\n  lg: 992px,\n  xl: 1200px\n);\n"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<div style=\"text-align:center\">\n  <h1>\n    Welcome to {{ title }}!\n  </h1>\n</div>\n<router-outlet></router-outlet>\n\n\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'Rice Book';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _auth_auth_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./auth/auth.component */ "./src/app/auth/auth.component.ts");
/* harmony import */ var _auth_login_login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./auth/login/login.component */ "./src/app/auth/login/login.component.ts");
/* harmony import */ var _auth_registeration_registeration_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./auth/registeration/registeration.component */ "./src/app/auth/registeration/registeration.component.ts");
/* harmony import */ var _main_main_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./main/main.component */ "./src/app/main/main.component.ts");
/* harmony import */ var _main_following_following_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./main/following/following.component */ "./src/app/main/following/following.component.ts");
/* harmony import */ var _main_headline_headline_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./main/headline/headline.component */ "./src/app/main/headline/headline.component.ts");
/* harmony import */ var _main_posts_posts_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./main/posts/posts.component */ "./src/app/main/posts/posts.component.ts");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/profile/profile.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _profile_current_info_current_info_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./profile/current-info/current-info.component */ "./src/app/profile/current-info/current-info.component.ts");
/* harmony import */ var _profile_update_info_update_info_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./profile/update-info/update-info.component */ "./src/app/profile/update-info/update-info.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_logger__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ngx-logger */ "./node_modules/ngx-logger/esm5/ngx-logger.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _auth_auth_component__WEBPACK_IMPORTED_MODULE_4__["AuthComponent"],
                _auth_login_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"],
                _auth_registeration_registeration_component__WEBPACK_IMPORTED_MODULE_6__["RegisterationComponent"],
                _main_main_component__WEBPACK_IMPORTED_MODULE_7__["MainComponent"],
                _main_following_following_component__WEBPACK_IMPORTED_MODULE_8__["FollowingComponent"],
                _main_headline_headline_component__WEBPACK_IMPORTED_MODULE_9__["HeadlineComponent"],
                _main_posts_posts_component__WEBPACK_IMPORTED_MODULE_10__["PostsComponent"],
                _profile_profile_component__WEBPACK_IMPORTED_MODULE_11__["ProfileComponent"],
                _profile_current_info_current_info_component__WEBPACK_IMPORTED_MODULE_13__["CurrentInfoComponent"],
                _profile_update_info_update_info_component__WEBPACK_IMPORTED_MODULE_14__["UpdateInfoComponent"]
            ],
            imports: [
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_12__["AppRoutingModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_16__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatSidenavModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_17__["FlexLayoutModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatGridListModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_18__["HttpClientModule"],
                ngx_logger__WEBPACK_IMPORTED_MODULE_19__["LoggerModule"].forRoot({ serverLoggingUrl: '/api/logs', level: ngx_logger__WEBPACK_IMPORTED_MODULE_19__["NgxLoggerLevel"].DEBUG })
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auth/auth.component.css":
/*!*****************************************!*\
  !*** ./src/app/auth/auth.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/auth/auth.component.html":
/*!******************************************!*\
  !*** ./src/app/auth/auth.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<!DOCTYPE html>-->\n<!--<html lang=\"en\">-->\n<!--<head>-->\n  <!--<title>Bootstrap Example</title>-->\n  <!--<meta charset=\"utf-8\">-->\n  <!--<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">-->\n  <!--<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\">-->\n  <!--<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>-->\n  <!--<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js\"></script>-->\n  <!--<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js\"></script>-->\n<!--</head>-->\n<!--<body>-->\n<div class=\"content-block\" id=\"contact\">\n\n<div class=\"demo-panel  \">\n\n<div class=\"container-fluid \">\n  <div class=\"container-fluid\">\n\n\n    <div class=\"row\">\n      <div class=\"col\" ><app-registeration></app-registeration></div>\n      <div class=\"col\"><app-login></app-login></div>\n    </div>\n  </div>\n</div>\n</div>\n</div>\n\n<!--</body>-->\n<!--</html>-->\n"

/***/ }),

/***/ "./src/app/auth/auth.component.ts":
/*!****************************************!*\
  !*** ./src/app/auth/auth.component.ts ***!
  \****************************************/
/*! exports provided: AuthComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthComponent", function() { return AuthComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthComponent = /** @class */ (function () {
    function AuthComponent(router) {
        this.router = router;
    }
    AuthComponent.prototype.ngOnInit = function () {
    };
    AuthComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-auth',
            template: __webpack_require__(/*! ./auth.component.html */ "./src/app/auth/auth.component.html"),
            styles: [__webpack_require__(/*! ./auth.component.css */ "./src/app/auth/auth.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AuthComponent);
    return AuthComponent;
}());



/***/ }),

/***/ "./src/app/auth/login/login.component.css":
/*!************************************************!*\
  !*** ./src/app/auth/login/login.component.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*.ng-valid[required] {*/\n  /*border-left: 5px solid #42A948; !* green *!*/\n  /*}*/\n  /*.ng-invalid {*/\n  /*border-left: 5px solid #a94442; !* red *!*/\n  /*}*/\n  div {\n  /*margin:0px auto;*/\n  /*width:300px;*/\n  height:50px;\n  /*background-color:red;*/\n  text-align:center;\n  /*font-size:32px;*/\n  /*color:white;*/\n}\n"

/***/ }),

/***/ "./src/app/auth/login/login.component.html":
/*!*************************************************!*\
  !*** ./src/app/auth/login/login.component.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  <h2>\n    Login\n  </h2>\n  <!--<h2>username: {{currentUser.username | uppercase}} </h2>-->\n  <!--<div><span>password: </span>{{currentUser.password}}</div>-->\n  <form #logForm=\"ngForm\" (ngSubmit)=\"onSubmit(logForm)\" novalidate>\n    <label>name:</label>\n    <input ngModel name=\"Username\" autocomplete=\"on\" required #UsernameValue=\"ngModel\" pattern=\"^[a-zA-Z][a-zA-Z0-9]*$\"><br>\n    <div class=\"alert\" [hidden]=\"UsernameValue.value!== ''\">\n      Please input username\n    </div>\n\n    <label>password:</label>\n    <input ngModel name=\"Password\" type=\"password\" autocomplete=\"on\" required #PasswordValue=\"ngModel\"><br>\n    <div class=\"alert\" [hidden]=\"PasswordValue.value!== ''\">\n      Please input password\n    </div>\n    <button name=\"submitButton\">Submit</button>\n\n\n    <div class=\"login_error\" [hidden]=\"!loginError\">\n      There is no such user or the password is not correct\n    </div>\n\n\n  </form>\n</div>\n"

/***/ }),

/***/ "./src/app/auth/login/login.component.ts":
/*!***********************************************!*\
  !*** ./src/app/auth/login/login.component.ts ***!
  \***********************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _user__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../user */ "./src/app/user.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _json_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../json-data.service */ "./src/app/json-data.service.ts");
/* harmony import */ var ngx_logger__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-logger */ "./node_modules/ngx-logger/esm5/ngx-logger.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginComponent = /** @class */ (function () {
    function LoginComponent(getDataService, router, logger) {
        var _this = this;
        this.getDataService = getDataService;
        this.router = router;
        this.logger = logger;
        this.loginError = false;
        this.loginUser = new _user__WEBPACK_IMPORTED_MODULE_1__["User"]();
        this.getDataService.getLoginPromise().subscribe(function (jsonUser) {
            _this.loginUser = jsonUser;
        });
        this.logger.debug('[login.component.ts : LoginComponent.constructor()] ' + 'in constructor');
    }
    LoginComponent.prototype.checkLoginUser = function (username, password) {
        this.logger.debug('[login.component.ts : LoginComponent.checkLoginUser()]' + ' username: ' + username);
        this.logger.debug('[login.component.ts : LoginComponent.checkLoginUser()]' + ' password: ' + password);
        this.logger.debug('[login.component.ts : LoginComponent.checkLoginUser()]' + ' this.loginUser: ' + JSON.stringify(this.loginUser));
        // console.log('username: ' + username);
        // console.log('password' + password);
        if (username === this.loginUser.username && password === this.loginUser.password) {
            this.logger.debug('[login.component.ts : LoginComponent.checkLoginUser()]' + ' correct login user ');
            return true;
        }
        this.logger.debug('[login.component.ts : LoginComponent.checkLoginUser()]' + ' error login user ');
        return false;
    };
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.onSubmit = function (form) {
        var _this = this;
        // if (form.valid) {
        // console.log(form.value);
        // localStorage.setItem('loginUser', form.value);
        // localStorage.setItem('testInfo', 'testInfoxxx');
        this.logger.debug('[login.component.ts : LoginComponent.onSubmit()] ' + 'in onSubmit()');
        if (this.checkLoginUser(form.value['Username'], form.value['Password'])) {
            this.router.navigate(['main']).then(function (suc) {
                _this.logger.debug('[login.component.ts : LoginComponent.()]' + 'redirect successful' + suc);
                _this.loginError = false;
                return true;
            }, function (fail) {
                _this.logger.debug('[login.component.ts : LoginComponent.()]' + 'can\'t redirect' + fail);
                return false;
            });
        }
        else {
            this.loginError = true;
        }
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/auth/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/auth/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [_json_data_service__WEBPACK_IMPORTED_MODULE_3__["JsonDataService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], ngx_logger__WEBPACK_IMPORTED_MODULE_4__["NGXLogger"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/auth/registeration/registeration.component.css":
/*!****************************************************************!*\
  !*** ./src/app/auth/registeration/registeration.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*.ng-valid[required] {*/\n  /*border-left: 5px solid #42A948; !* green *!*/\n  /*}*/\n  /*.ng-invalid {*/\n  /*border-left: 5px solid #a94442; !* red *!*/\n  /*}*/\n  div {\n  /*margin:0px auto;*/\n  /*width:300px;*/\n  height:50px;\n  /*background-color:red;*/\n  text-align:center;\n  /*font-size:32px;*/\n  /*color:white;*/\n}\n"

/***/ }),

/***/ "./src/app/auth/registeration/registeration.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/auth/registeration/registeration.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  <h2>\n    Registration\n  </h2>\n  <form #logForm=\"ngForm\" (ngSubmit)=\"onSubmit(logForm)\" novalidate>\n    <label>user name*:</label>\n    <input ngModel name=\"Username\"  autocomplete=\"on\" required #UsernameValue=\"ngModel\" pattern=\"^[a-zA-Z][a-zA-Z0-9]*$\"><br>\n    <div class=\"alert\" [hidden]=\"UsernameValue.valid || UsernameValue.pristine\">\n      Please only use letter and number as username and start with letter\n    </div>\n    <br>\n\n\n    <label>email address*:</label>\n    <input ngModel name=\"email\" autocomplete=\"on\" required placeholder=\"a@b.com\" #EmailValue=\"ngModel\"\n           pattern=\"^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$\"><br>\n    <div class=\"alert\" [hidden]=\"EmailValue.valid || EmailValue.pristine\">\n      Please enter valid email\n    </div>\n    <br>\n\n\n    <label>phone number*:</label>\n    <input ngModel name=\"phone\" autocomplete=\"on\" required placeholder=\"123-123-1234\" #PhoneValue=\"ngModel\" pattern=\"^\\d{3}-\\d{3}-\\d{4}$\"><br>\n    <div class=\"alert\" [hidden]=\"PhoneValue.valid || PhoneValue.pristine\">\n      Please enter valid phone number\n    </div>\n    <br>\n\n\n    <label>date of birth*:</label>\n    <input ngModel name=\"birthday\" autocomplete=\"on\" required type=\"date\" #BirthdayValue=\"ngModel\"><br>\n    <div class=\"alert\" [hidden]=\"BirthdayValue.valid || BirthdayValue.pristine\">\n      Please enter valid date of birth\n    </div>\n    <br>\n\n\n    <label>zipcode*:</label>\n    <input ngModel name=\"zipcode\" autocomplete=\"on\" required type=\"text\" placeholder=\"00000\" #ZipcodeValue=\"ngModel\"\n           pattern=\"^\\d{5}$\"><br>\n    <div class=\"alert\" [hidden]=\"ZipcodeValue.valid || ZipcodeValue.pristine\">\n      Please enter valid zipcode\n    </div>\n    <br>\n\n\n    <label>password:</label>\n    <input ngModel name=\"Password\" type=\"password\" autocomplete=\"on\" required #PasswordValue=\"ngModel\"><br>\n\n    <label>password confirmation*:</label>\n    <input ngModel name=\"passwordConfirmation\" type=\"password\" autocomplete=\"on\" required #PasswordConfirmValue=\"ngModel\"><br>\n    <div class=\"alert\" *ngIf=\"PasswordValue.value !== PasswordConfirmValue.value\">\n      Password and password confirmation doesn't equal\n    </div>\n\n    <button>Submit</button>\n    <!--<button (click)=\"onSubmit()\">Submit</button>-->\n  </form>\n</div>\n"

/***/ }),

/***/ "./src/app/auth/registeration/registeration.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/auth/registeration/registeration.component.ts ***!
  \***************************************************************/
/*! exports provided: RegisterationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterationComponent", function() { return RegisterationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _json_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../json-data.service */ "./src/app/json-data.service.ts");
/* harmony import */ var ngx_logger__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-logger */ "./node_modules/ngx-logger/esm5/ngx-logger.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RegisterationComponent = /** @class */ (function () {
    function RegisterationComponent(router, jsonDataService, logger) {
        this.router = router;
        this.jsonDataService = jsonDataService;
        this.logger = logger;
    }
    RegisterationComponent.prototype.ngOnInit = function () {
    };
    RegisterationComponent.prototype.onSubmit = function (form) {
        var _this = this;
        // if (form.valid) {
        // console.log(form.value);
        // localStorage.setItem('loginUser', form.value);
        // localStorage.setItem('testInfo', 'testInfoxxx');
        if (form.valid) {
            this.router.navigate(['/main']).then(function (suc) {
                _this.logger.debug('[login.component.ts : LoginComponent.()]' + suc);
            }, function (fail) {
                _this.logger.debug('[login.component.ts : LoginComponent.()]' + fail);
            });
        }
    };
    RegisterationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-registeration',
            template: __webpack_require__(/*! ./registeration.component.html */ "./src/app/auth/registeration/registeration.component.html"),
            styles: [__webpack_require__(/*! ./registeration.component.css */ "./src/app/auth/registeration/registeration.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _json_data_service__WEBPACK_IMPORTED_MODULE_2__["JsonDataService"], ngx_logger__WEBPACK_IMPORTED_MODULE_3__["NGXLogger"]])
    ], RegisterationComponent);
    return RegisterationComponent;
}());



/***/ }),

/***/ "./src/app/json-data.service.ts":
/*!**************************************!*\
  !*** ./src/app/json-data.service.ts ***!
  \**************************************/
/*! exports provided: JsonDataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JsonDataService", function() { return JsonDataService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_logger__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-logger */ "./node_modules/ngx-logger/esm5/ngx-logger.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import 'rxjs/add/operator/mergeMap';
// import {flatMap, mergeMap} from 'rxjs/operators';
var JsonDataService = /** @class */ (function () {
    // dataUpdated: EventEmitter = new EventEmitter();
    function JsonDataService(http, logger) {
        var _this = this;
        this.http = http;
        this.logger = logger;
        this.allUsers = [];
        this.followedUsersIdList = [11, 12, 13];
        this.allPosts = [];
        this.subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.loginUser = undefined;
        // console.log('in getAllUsers ');
        this.http.get('./assets/profile.json').subscribe(function (jsonUser) {
            // console.log('jsonUser= ' + jsonUser);
            _this.loginUser = jsonUser;
            _this.logger.debug('[json-data.service.ts : JsonDataService.()] ' + 'this.loginUser: ' + _this.loginUser);
            // console.log(this.loginUser['username']);
            // console.log(this.loginUser['birthday']);
            // console.log(this.loginUser['phone']);
            // console.log(this.loginUser['phone']);
            // console.log(this.loginUser['email']);
        });
        //// console.log('in getAllUsers ');
        this.http.get('./assets/users.json').subscribe(function (usersWithString) {
            //// console.log(usersWithString);
            usersWithString.forEach(function (userWithString) {
                var tempUser = {
                    id: parseInt(userWithString['id'], 10),
                    username: userWithString['name'],
                    status: userWithString['status'],
                    image: userWithString['image'],
                    // todo change to real password
                    password: '',
                    passwordConfirmation: '',
                    zipcode: '00000',
                    birthday: new Date(),
                    phone: '111-111-1111',
                    email: 'a@b.com'
                };
                //// console.log(tempUser);
                _this.allUsers.push(tempUser);
            });
            //// console.log(this.allUsers);
        });
        this.http.get('./assets/articles.json').subscribe(function (postsWithString) {
            //// console.log(usersWithString);
            postsWithString.forEach(function (postWithString) {
                var tempPost = {
                    id: parseInt(postWithString['id'], 10),
                    title: postWithString['title'],
                    author: postWithString['author'],
                    text: postWithString['text'],
                    image: postWithString['image'],
                    timeStamp: postWithString['timeStamp']
                };
                //// console.log(tempUser);
                _this.allPosts.push(tempPost);
                _this.logger.debug('[json-data.service.ts : JsonDataService.()] ' + 'this.allPosts: ' + _this.allPosts.length);
            });
            // return this.allPosts;
            //// console.log(this.allUsers);
        });
    }
    JsonDataService.prototype.updateData = function () {
        this.logger.debug('[json-data.service.ts : JsonDataService.sendMessage()] ' + 'send message to service');
        this.subject.next();
    };
    JsonDataService.prototype.dataUpdated = function () {
        // this.logger.debug('[json-data.service.ts : JsonDataService.dataUpdated()] '+'notify service date');
        return this.subject.asObservable();
    };
    JsonDataService.prototype.getAllUsers = function () {
        this.logger.debug('[json-data.service.ts : JsonDataService.getAllUsers()] ' + 'this.allUsers: ' + this.allUsers.length);
        return this.allUsers;
    };
    JsonDataService.prototype.getAllPosts = function () {
        this.logger.debug('[json-data.service.ts : JsonDataService.getAllPosts()] ' + 'this.allPosts: ' + this.allPosts.length);
        return this.allPosts;
    };
    JsonDataService.prototype.getLoginUser = function () {
        this.logger.debug('[json-data.service.ts : JsonDataService.getAllPosts()] ' + 'this.loginUser: ' + this.loginUser);
        return this.loginUser;
    };
    //
    // async getAllPosts() {
    //   //// console.log('in getAllUsers ');
    //   const response = <TempPost[]>await this.http.get('./assets/articles.json').toPromise();
    //
    //   response.forEach(postWithString => {
    //     const tempPost: Post = {
    //       id: parseInt(postWithString['id'], 10),
    //       title: postWithString['title'],
    //       author: postWithString['author'],
    //       text: postWithString['text'],
    //       image: postWithString['image'],
    //       timeStamp: postWithString['timeStamp']
    //     };
    //     //// console.log(tempUser);
    //     this.allPosts.push(tempPost);
    //     this.logger.debug('[json-data.service.ts : JsonDataService.getAllPosts()] ' + 'this.allPosts: ' + this.allPosts.length);
    //
    //   });
    //
    //
    //   return new Promise(function (resolve, reject) {
    //     resolve(this.allPosts);
    //   });
    //   //// console.log(this.allUsers);
    //
    //
    // }
    JsonDataService.prototype.getAllPostsObservable = function () {
        var _this = this;
        return this.http.get('./assets/articles.json').toPromise().then(function (postsWithString) {
            var tempPosts = [];
            //// console.log(usersWithString);
            postsWithString.forEach(function (postWithString) {
                var tempPost = {
                    id: parseInt(postWithString['id'], 10),
                    title: postWithString['title'],
                    author: postWithString['author'],
                    text: postWithString['text'],
                    image: postWithString['image'],
                    timeStamp: postWithString['timeStamp']
                };
                //// console.log(tempUser);
                tempPosts.push(tempPost);
                _this.logger.debug('[json-data.service.ts : JsonDataService.getAllPosts()] ' + 'tempPosts: ' + tempPosts);
            });
            return tempPosts;
            //// console.log(this.allUsers);
        });
    };
    JsonDataService.prototype.getLoginUserObservable = function () {
        var _this = this;
        return this.http.get('./assets/profile.json').toPromise().then(function (jsonUser) {
            if (_this.loginUser !== undefined) {
                return _this.loginUser;
            }
            // console.log('jsonUser= ' + jsonUser);
            _this.loginUser = jsonUser;
            _this.logger.debug('[json-data.service.ts : JsonDataService.()] ' + 'this.loginUser: ' + _this.loginUser);
            return _this.loginUser;
        });
    };
    JsonDataService.prototype.getFollowedUsersIdList = function () {
        return this.followedUsersIdList;
    };
    JsonDataService.prototype.getAllUsersObservable = function () {
        var _this = this;
        return this.http.get('./assets/articles.json').toPromise().then(function (usersWithString) {
            var tempUsers = [];
            //// console.log(usersWithString);
            usersWithString.forEach(function (userWithString) {
                var tempUser = {
                    id: parseInt(userWithString['id'], 10),
                    username: userWithString['name'],
                    status: userWithString['status'],
                    image: userWithString['image'],
                    // todo change to real password
                    password: '',
                    passwordConfirmation: '',
                    zipcode: '00000',
                    birthday: new Date(),
                    phone: '111-111-1111',
                    email: 'a@b.com'
                };
                //// console.log(tempUser);
                _this.allUsers.push(tempUser);
                tempUsers.push(tempUser);
            });
            return _this.allUsers;
        });
    };
    JsonDataService.prototype.getLoginPromise = function () {
        return this.http.get('./assets/profile.json');
    };
    JsonDataService.prototype.putLoginUser = function (user) {
        //// console.log('in putLoginUser, user= ');
        // this.http.get('./assets/profile.json').subscribe(data =>// console.log('data= ' + JSON.stringify(data)));
        //// console.log(user);
        //// console.log(JSON.stringify(user));
        // this.http.post('./assets/profile.json', user).subscribe(
        //   data => {
        //    // console.log('PUT Request is successful ', data);
        //   },
        //   error => {
        //    // console.log('Rrror', error);
        //   }
        // );
    };
    JsonDataService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root',
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], ngx_logger__WEBPACK_IMPORTED_MODULE_2__["NGXLogger"]])
    ], JsonDataService);
    return JsonDataService;
}());



/***/ }),

/***/ "./src/app/main/allPosts.ts":
/*!**********************************!*\
  !*** ./src/app/main/allPosts.ts ***!
  \**********************************/
/*! exports provided: currentPostIdList */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "currentPostIdList", function() { return currentPostIdList; });
//
// export const allPosts: Post[] = [
//     {
//       id: 200000,
//       title: 'City',
//       author: '',
//       image: 'https://image.shutterstock.com/display_pic_with_logo/3798320/348384155/stock-photo-new-york-city-th-a' +
//         've-vertical-348384155.jpg',
//       text: 'A city is a large human settlement. Cities generally have extensive systems for housing,\n' +
//         '                  transportation, sanitation, utilities, land use, and communication. Their density facilitates\n' +
//         '                  interaction between people, government organizations and businesses, sometimes benefiting different\n' +
//         '                  parties in the process.',
//     },
//     {
//       id: 200001,
//       title: 'Nature',
//       author: '',
//       image: 'https://image.shutterstock.com/display_' +
//         'pic_with_logo/688576/407021107/stock-photo-mountains-during-sunset-beautiful-natural-landscape-in-the-summer-time-407021107.jpg',
//       text: 'Nature, in the broadest sense, is the natural, physical, or material world or universe. "Nature" can\n' +
//         '                  refer to the phenomena of the physical world, and also to life in general. The study of nature is a\n' +
//         '                  large, if not the only, part of science. Although humans are part of nature, human activity is often\n' +
//         '                  understood as a separate category from other natural phenomena.',
//     },
//
//     {
//       id: 200002,
//       title: 'Business',
//       author: '',
//       image: 'https://image.shutterstock.com/display_pic_with_logo/2117717/430168801/' +
//         'stock-photo-business-people-meeting-conference-discussion-corporate-concept-430168801.jpg',
//       text: 'Business is the activity of making one\'s living or making money by producing or buying and selling\n' +
//         '                  products (goods and services). Simply put, it is "any activity or enterprise entered into for\n' +
//         '                  profit. It\n' +
//         '                  does not mean it is a company, a corporation, partnership, or have any such formal organization, but\n' +
//         '                  it\n' +
//         '                  can range from a street peddler to General Motors." The term is also often used colloquially (but\n' +
//         '                  not by\n' +
//         '                  lawyers or public officials) to refer to a company, but this article will not deal with that sense\n' +
//         '                  of\n' +
//         '                  the word.',
//     },
//
//
//     {
//       id: 200003,
//       title: 'Girl',
//       author: '',
//       image: 'https://image.shutterstock.com/display_pic_with_logo' +
//         '/2967241/623804987/' +
//         'stock-photo-headshot-portrait-of-happy-ginger-girl-with-freckles-smiling-looking-at-camera-white-background-623804987.jpg',
//       text: 'A girl is a young female human, usually a child or an adolescent. When she becomes an adult, she is\n' +
//         '                  described as a woman. The term girl may also be used to mean a young woman, and is sometimes used as\n' +
//         '                  a synonym for daughter. Girl may also be a term of endearment used by an adult, usually a woman, to\n' +
//         '                  designate adult female friends.',
//     },
//
//     {
//       id: 200004,
//       title: 'Computer',
//       author: '',
//       image: 'https://image.shutterstock.com/display_pic_with_logo' +
//         '/897283/340152863/stock-photo-laptop-with-blank-screen-on-table-340152863.jpg',
//       text: 'A computer is a device that can be instructed to carry out sequences of arithmetic or logical\n' +
//         '                  operations\n' +
//         '                  automatically via computer programming. Modern computers have the ability to follow generalized sets\n' +
//         '                  of\n' +
//         '                  operations, called programs. These programs enable computers to perform an extremely wide range of\n' +
//         '                  tasks.',
//     },
//
//     {
//       id: 200005,
//       title: 'Food',
//       author: '',
//       image: 'https://image.shutterstock.com/display_pic_with_logo' +
//         '/136306/722718082/' +
//         'stock-photo-healthy-food-clean-eating-selection-fruit-vegetable-seeds-superfood-cereals-leaf-vegetable-on-722718082.jpg',
//       text: 'Food is any substance consumed to provide nutritional support for an organism. It is usually of plant\n' +
//         '                  or\n' +
//         '                  animal origin, and contains essential nutrients, such as carbohydrates, fats, proteins, vitamins, or\n' +
//         '                  minerals. The substance is ingested by an organism and assimilated by the organism\'s cells to\n' +
//         '                  provide\n' +
//         '                  energy, maintain life, or stimulate growth.',
//     },
//
//     {
//       id: 200006,
//       title: 'Education',
//       author: '',
//       image: 'https://image.shutterstock.com/z/' +
//         'stock-photo-book-in-library-with-old-open-textbook-stack-piles-of-literature-text-archive-on-reading-desk-and-785109361.jpg',
//       text: 'Education is the process of facilitating learning, or the acquisition of knowledge, skills, values, beliefs, and habits',
//     },
//
//     {
//       id: 200007,
//       title: 'Sculpture',
//       author: '',
//       image: 'https://image.shutterstock.com/z/' +
//         'stock-photo-white-gypsum-statue-of-apollo-s-head-766744111.jpg',
//       text: 'Sculpture is the branch of the visual arts that operates in three dimensions. It is one of the plastic arts.',
//     }
//
//   ]
// ;
var currentPostIdList = [20000, 200001, 200002, 20003, 20004, 20005, 20006, 20007];


/***/ }),

/***/ "./src/app/main/following/following.component.css":
/*!********************************************************!*\
  !*** ./src/app/main/following/following.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* HeroesComponent's private CSS styles */\n.heroes {\n  margin: 0 0 2em 0;\n  list-style-type: none;\n  padding: 0;\n  width: 15em;\n}\n.heroes li {\n  position: relative;\n  cursor: pointer;\n  background-color: #EEE;\n  margin: .5em;\n  padding: .3em 0;\n  height: 1.6em;\n  border-radius: 4px;\n}\n.heroes li:hover {\n  color: #607D8B;\n  background-color: #DDD;\n  left: .1em;\n}\n.heroes a {\n  color: #888;\n  text-decoration: none;\n  position: relative;\n  display: block;\n  width: 250px;\n}\n.heroes a:hover {\n  color:#607D8B;\n}\n.heroes .badge {\n  display: inline-block;\n  font-size: small;\n  color: white;\n  padding: 0.8em 0.7em 0 0.7em;\n  background-color: #607D8B;\n  line-height: 1em;\n  position: relative;\n  left: -1px;\n  top: -4px;\n  height: 1.8em;\n  min-width: 16px;\n  text-align: right;\n  margin-right: .8em;\n  border-radius: 4px 0 0 4px;\n}\nbutton {\n  background-color: #eee;\n  border: none;\n  padding: 5px 10px;\n  border-radius: 4px;\n  cursor: pointer;\n  cursor: hand;\n  font-family: Arial;\n}\nbutton:hover {\n  background-color: #cfd8dc;\n}\nbutton.delete {\n  position: relative;\n  left: 194px;\n  top: -32px;\n  background-color: gray !important;\n  color: white;\n}\n\n"

/***/ }),

/***/ "./src/app/main/following/following.component.html":
/*!*********************************************************!*\
  !*** ./src/app/main/following/following.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<ul class=\"heroes\">-->\n  <!--<li>-->\n    <!--<app-user-followed></app-user-followed>-->\n  <!--</li>-->\n\n\n  <!--<li *ngFor=\"let hero of heroes\">-->\n    <!--<span class=\"badge\">{{hero.id}}</span> {{hero.title}}-->\n  <!--</li>-->\n<!--</ul>-->\n<mat-card class=\"mat-elevation-z0\" *ngIf=\"(isLoggedUser) || followedUsersList.indexOf(userDisplayed.id)>-1\">\n\n<div class=\"grid_div\">\n  <img src=\"{{userDisplayed.image}}\"\n       class=\"card-image img-responsive\" style=\"max-width: 100px; max-height: 100px; width:100%;height: 100%\">\n  <h2 class=\"post_title\">{{userDisplayed.username}}</h2>\n  <p id=\"userHeadline\">{{userDisplayed.status}}</p>\n  <button name=\"unfollowButton\" *ngIf=\"!(isLoggedUser)\" type=\"button\" class=\"stop_button\" (click)=\"unfollow()\">Unfollow</button>\n\n  <input *ngIf=\"isLoggedUser\" [(ngModel)]=\"inputHeadline\" style=\"width: 100%\" type=\"text\">\n\n  <button name=\"updateButton\" *ngIf=\"isLoggedUser\" type=\"button\" class=\"stop_button\" (click)=\"updateHeadline()\">Update</button>\n</div>\n\n</mat-card>\n\n\n\n\n"

/***/ }),

/***/ "./src/app/main/following/following.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/main/following/following.component.ts ***!
  \*******************************************************/
/*! exports provided: FollowingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FollowingComponent", function() { return FollowingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _user__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../user */ "./src/app/user.ts");
/* harmony import */ var _json_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../json-data.service */ "./src/app/json-data.service.ts");
/* harmony import */ var _node_modules_ngx_logger__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/ngx-logger */ "./node_modules/ngx-logger/esm5/ngx-logger.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// import {followedUsersIdList} from '../allUsers';



var FollowingComponent = /** @class */ (function () {
    function FollowingComponent(getDataService, logger) {
        this.getDataService = getDataService;
        this.logger = logger;
        this.headlineChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.updateHeadlineEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.followedUsersList = this.getDataService.getFollowedUsersIdList();
    }
    Object.defineProperty(FollowingComponent.prototype, "headline", {
        get: function () {
            return this.headlineValue;
        },
        set: function (val) {
            this.headlineValue = val;
            this.headlineChange.emit(this.inputHeadline);
        },
        enumerable: true,
        configurable: true
    });
    FollowingComponent.prototype.ngOnInit = function () {
        //// console.log(followedUsersIdList);
        //// console.log(this.userDisplayed);
    };
    FollowingComponent.prototype.unfollow = function () {
        this.followedUsersList.splice(this.followedUsersList.indexOf(this.userDisplayed.id), 1);
        this.logger.debug('[following.component.ts : FollowingComponent.unfollow()] ' + 'followed user list: ' + this.followedUsersList);
        this.getDataService.updateData();
        //// console.log('in unfollow, ' + followedUsersIdList);
    };
    FollowingComponent.prototype.updateHeadline = function () {
        //// console.log(this.inputHeadline);
        //// console.log(document.getElementById('userHeadline'));
        if (this.inputHeadline !== undefined) {
            document.getElementById('userHeadline').innerHTML = this.inputHeadline;
            // TODO
            this.logger.debug('[following.component.ts : FollowingComponent.updateHeadline()] ' + 'get headline: ' + this.inputHeadline);
            this.userDisplayed.status = this.inputHeadline;
            this.updateHeadlineEvent.emit(this.inputHeadline);
            this.logger.debug('[following.component.ts : FollowingComponent.updateHeadline()] ' + 'get headline: ' + this.inputHeadline);
            localStorage.setItem('headline', this.inputHeadline);
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _user__WEBPACK_IMPORTED_MODULE_1__["User"])
    ], FollowingComponent.prototype, "userDisplayed", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], FollowingComponent.prototype, "isLoggedUser", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], FollowingComponent.prototype, "headline", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], FollowingComponent.prototype, "headlineChange", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], FollowingComponent.prototype, "updateHeadlineEvent", void 0);
    FollowingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-following',
            template: __webpack_require__(/*! ./following.component.html */ "./src/app/main/following/following.component.html"),
            styles: [__webpack_require__(/*! ./following.component.css */ "./src/app/main/following/following.component.css")]
        }),
        __metadata("design:paramtypes", [_json_data_service__WEBPACK_IMPORTED_MODULE_2__["JsonDataService"], _node_modules_ngx_logger__WEBPACK_IMPORTED_MODULE_3__["NGXLogger"]])
    ], FollowingComponent);
    return FollowingComponent;
}());



/***/ }),

/***/ "./src/app/main/headline/headline.component.css":
/*!******************************************************!*\
  !*** ./src/app/main/headline/headline.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".child-css{\n  width: 100%;\n  height: 100%;\n}\n\n.bottom{\n  alignment: bottom;\n}\n\n.wholeWidth{\n  width: 100%;\n}\n\n\n"

/***/ }),

/***/ "./src/app/main/headline/headline.component.html":
/*!*******************************************************!*\
  !*** ./src/app/main/headline/headline.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-grid-list cols=\"4\">\n  <mat-grid-tile [colspan]=\"2\">\n\n    <div style=\"display: block\">\n      <div><input type=\"file\" id=\"mapSelector\"></div>\n      <div style=\"margin:5px\">\n        <button type=\"button\" class=\"stop_button\" (click)=\"cancel()\">Cancel</button>\n      </div>\n\n    </div>\n    <div style=\"margin: 5px\">\n        <textarea id=\"textarea\" rows=\"4\" cols=\"50\" class=\"wholeWidth\">\n          Input your post here!\n        </textarea>\n      <button type=\"button\" class=\"stop_button\" (click)=\"post()\">Post</button>\n\n    </div>\n  </mat-grid-tile>\n\n  <mat-grid-tile [colspan]=\"2\">\n    <img src=\"https://s1.ax1x.com/2018/10/06/i8bEkD.png\" style=\"width: 100%\">\n  </mat-grid-tile>\n</mat-grid-list>\n"

/***/ }),

/***/ "./src/app/main/headline/headline.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/main/headline/headline.component.ts ***!
  \*****************************************************/
/*! exports provided: HeadlineComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeadlineComponent", function() { return HeadlineComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _json_data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../json-data.service */ "./src/app/json-data.service.ts");
/* harmony import */ var _allPosts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../allPosts */ "./src/app/main/allPosts.ts");
/* harmony import */ var _user__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../user */ "./src/app/user.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HeadlineComponent = /** @class */ (function () {
    function HeadlineComponent(getDataService) {
        var _this = this;
        this.getDataService = getDataService;
        this.id = 200000;
        this.currentPostIdList = _allPosts__WEBPACK_IMPORTED_MODULE_2__["currentPostIdList"];
        this.loginUser = new _user__WEBPACK_IMPORTED_MODULE_3__["User"]();
        this.allPosts = this.getDataService.getAllPosts();
        this.getDataService.getLoginPromise().subscribe(function (jsonUser) {
            _this.loginUser = jsonUser;
        });
    }
    HeadlineComponent.prototype.ngOnInit = function () {
        //// console.log('id= ' + this.id);
        //// console.log(this.allPosts);
    };
    HeadlineComponent.prototype.cancel = function () {
        // if (this.id < 200000) {
        //   this.allPosts.splice(0, 1);
        //   this.id += 1;
        // }
        // @ts-ignore
        // const post = document.getElementById('textarea').value;
        //// console.log('post= ' + post);
        // @ts-ignore
        document.getElementById('textarea').value = '';
    };
    HeadlineComponent.prototype.post = function () {
        // @ts-ignore
        var post = document.getElementById('textarea').value;
        //// console.log('post= ' + post);
        if (post !== '' && post !== undefined) {
            var newPost = {
                id: this.id,
                title: 'My post',
                // author: 'zx17',
                image: 'https://smallbusiness.fedex.com/content/dam/SMB/SMAC/Wide/0002_Talking%20Small-assets-C.jpg',
                author: this.loginUser.username,
                text: post.toString(),
                timeStamp: new Date()
            };
            this.id -= 1;
            this.allPosts.splice(0, 0, newPost);
            // @ts-ignore
            document.getElementById('textarea').value = '';
        }
        //// console.log(this.allPosts);
    };
    HeadlineComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-headline',
            template: __webpack_require__(/*! ./headline.component.html */ "./src/app/main/headline/headline.component.html"),
            styles: [__webpack_require__(/*! ./headline.component.css */ "./src/app/main/headline/headline.component.css")]
        }),
        __metadata("design:paramtypes", [_json_data_service__WEBPACK_IMPORTED_MODULE_1__["JsonDataService"]])
    ], HeadlineComponent);
    return HeadlineComponent;
}());



/***/ }),

/***/ "./src/app/main/main.component.css":
/*!*****************************************!*\
  !*** ./src/app/main/main.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n.example-container {\n  width: 100%;\n  height: 900px;\n  border: 1px hidden rgba(111, 111, 111, 0.50);\n}\n\n.example-sidenav-content {\n  display: flex;\n\n  height: 100%;\n  align-items: center;\n  justify-content: center;\n}\n\n.example-sidenav {\n  width: 150px;\n  padding: 20px;\n  border: 1px solid\n}\n\n.demo-panel {\n  margin: 30px;\n}\n\nhr {\n  display: block;\n  border-width: 3px;\n}\n\nmat-card {\n  border: hidden;\n}\n\nmat-card-content{\n  border: hidden;\n}\n\n/*mat-grid-tile {*/\n\n/*background: lightblue;*/\n\n/*}*/\n\n\n"

/***/ }),

/***/ "./src/app/main/main.component.html":
/*!******************************************!*\
  !*** ./src/app/main/main.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"demo-panel  \">\n  <mat-sidenav-container class=\"example-container\" class=\"mat-elevation-z0\">\n\n    <mat-sidenav opened mode=\"side\" class=\"example-sidenav\">\n      <!--<h2>Followed Users</h2>-->\n      <nav>\n        <ul>\n          <li><a class=\"logOut_Button\" routerLink=\"/auth\" (click)=\"logout()\">Log Out</a></li>\n          <li><a routerLink=\"/profile\">Profile</a></li>\n        </ul>\n      </nav>\n      <app-following *ngIf=\"loginUser\"  [userDisplayed]=\"loginUser\" [isLoggedUser]=\"true\"  (updateHeadlineEvent)=\"updateLoginUserHeadline($event)\" style=\"width: 100%\">\n      </app-following>\n\n\n        <hr>\n      {{title}}\n        <hr>\n        <app-following *ngFor=\"let user of allUsers\" [userDisplayed]=\"user\" [isLoggedUser]=\"false\"  [(headline)]=\"title\"  >\n        </app-following>\n\n      <br>\n\n      <input  name=\"followerInput\"  [(ngModel)]=\"inputFollowedName\" style=\"width: 100%\" type=\"text\">\n\n      <button name=\"addButton\" type=\"button\" class=\"stop_button\" (click)=\"addFollowedUser()\">Add</button>\n      <div class=\"addFollowerMsg\"  >{{addFollowerMsg}}</div>\n    </mat-sidenav>\n\n\n\n\n\n    <mat-card class=\"example-container\" class=\"mat-elevation-z0\">\n\n      <mat-card-content class=\"mat-elevation-z0\">\n        <app-headline></app-headline>\n        <app-posts></app-posts>\n\n      </mat-card-content>\n    </mat-card>\n\n  </mat-sidenav-container>\n\n</div>\n\n"

/***/ }),

/***/ "./src/app/main/main.component.ts":
/*!****************************************!*\
  !*** ./src/app/main/main.component.ts ***!
  \****************************************/
/*! exports provided: MainComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainComponent", function() { return MainComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _user__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../user */ "./src/app/user.ts");
/* harmony import */ var _json_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../json-data.service */ "./src/app/json-data.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _node_modules_ngx_logger__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../node_modules/ngx-logger */ "./node_modules/ngx-logger/esm5/ngx-logger.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};

// import {followedUsersIdList} from './allUsers';




var MainComponent = /** @class */ (function () {
    function MainComponent(getDataService, location, logger) {
        this.getDataService = getDataService;
        this.location = location;
        this.logger = logger;
        this.showAddFollowMsg = false;
        this.allUsers = [];
        this.followedUserIdList = [];
        this.id = 0;
        this.addFollowerMsg = '';
        this.loginUser = new _user__WEBPACK_IMPORTED_MODULE_1__["User"]();
        //// console.log('in constructor, ' );
        //// console.log(this.allUsers);
        //// console.log(this.allUsers.length);
        //// console.log(this.id);
        this.logger.debug('[main.component.ts : MainComponent.constructor()] ' + 'constructor start');
    }
    MainComponent.prototype.logout = function () {
        localStorage.clear();
        this.logger.debug('[main.component.ts : MainComponent.logout()] ');
    };
    MainComponent.prototype.addFollowedUser = function () {
        var i = 0;
        for (i = 0; i < this.allUsers.length; i++) {
            if (this.allUsers[i].username == this.inputFollowedName) {
                var j = 0;
                for (j = 0; j < this.followedUserIdList.length; j++) {
                    if (this.allUsers[i].id === this.followedUserIdList[j]) {
                        // output already followed
                        // this.showAddFollowMsg = true;
                        this.addFollowerMsg = 'already followed this user';
                        return;
                    }
                }
                this.followedUserIdList.push(this.allUsers[i].id);
                this.getDataService.updateData();
                this.addFollowerMsg = '';
                return;
            }
        }
        //output no such user
        this.addFollowerMsg = 'no such user';
        // this.id = 11 + this.allUsers.length;
        //
        // if (this.inputFollowedName !== '' && this.inputFollowedName !== undefined) {
        //   const newFollowedUser: User = {
        //     id: this.id,
        //     username: this.inputFollowedName,
        //     password: '',
        //     passwordConfirmation: '',
        //     zipcode: '00000',
        //     birthday: new Date(),
        //     phone: '111-111-1111',
        //     email: 'a@b.com',
        //     image: 'http://www.clker.com/' +
        //       'cliparts/q/a/C/u/Z/R/red-basic-female-symbol-hi.png',
        //     status: 'hahaha!'
        //   };
        //   this.followedUserIdList.push(this.id);
        //   this.allUsers.push(newFollowedUser);
        //   this.getDataService.updateData();
        // }
        //// console.log('in addFollow, ' );
        //// console.log(this.allUsers);
        //// console.log('in addFollow, ' + this.followedUserIdList);
    };
    MainComponent.prototype.updateLoginUserHeadline = function (headline) {
        this.logger.debug('[main.component.ts : MainComponent.updateLoginuserHeadLine()] ' + 'loginuser: ' + this.loginUser);
        this.loginUser.status = headline;
        this.logger.debug('[main.component.ts : MainComponent.updateLoginuserHeadLine()] ' + 'loginuser.status: ' + this.loginUser.status);
        this.logger.debug('[main.component.ts : MainComponent.updateLoginuserHeadLine()] ' +
            'getLoginUser.status: ' + this.getDataService.getLoginUser().status);
    };
    MainComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.logger.debug('[main.component.ts : MainComponent.ngOnInit()] ' + 'ngOnInit start');
                // this.getDataService.getAllUsers();
                this.getDataService.getAllUsersObservable().then(function (tempUsers) {
                    _this.allUsers = tempUsers;
                });
                // this.loginUser = this.getDataService.getLoginUser();
                this.getDataService.getLoginUserObservable().then(function (jsonUser) {
                    _this.loginUser = JSON.parse(JSON.stringify(jsonUser));
                    var headline = localStorage.getItem('headline');
                    if (headline !== '' && headline !== undefined && headline !== null) {
                        _this.loginUser.status = headline;
                    }
                    _this.logger.debug('[main.component.ts : MainComponent.()] ' + 'headline ' + headline);
                    _this.logger.debug('[main.component.ts : MainComponent.()] ' + 'loginUser ' + _this.loginUser.status);
                });
                // this.loginUser = this.getDataService.getLoginUser();
                this.followedUserIdList = this.getDataService.getFollowedUsersIdList();
                (this.id = 11 + this.allUsers.length);
                return [2 /*return*/];
            });
        });
    };
    MainComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-main',
            template: __webpack_require__(/*! ./main.component.html */ "./src/app/main/main.component.html"),
            styles: [__webpack_require__(/*! ./main.component.css */ "./src/app/main/main.component.css")],
        }),
        __metadata("design:paramtypes", [_json_data_service__WEBPACK_IMPORTED_MODULE_2__["JsonDataService"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"], _node_modules_ngx_logger__WEBPACK_IMPORTED_MODULE_4__["NGXLogger"]])
    ], MainComponent);
    return MainComponent;
}());



/***/ }),

/***/ "./src/app/main/posts/posts.component.css":
/*!************************************************!*\
  !*** ./src/app/main/posts/posts.component.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\nmat-card{\n  width: 100%;\n  height: 100%;\n}\nmat-grid-list{\n  border: hidden;\n}\nmat-card-actions{\n  alignment: center;\n}\n"

/***/ }),

/***/ "./src/app/main/posts/posts.component.html":
/*!*************************************************!*\
  !*** ./src/app/main/posts/posts.component.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<mat-form-field class=\"search-form \" style=\"  width: 100%;\">\n  <input [(ngModel)]=\"searchText\" matInput placeholder=\"Search Post\" type=\"text\">\n</mat-form-field>\n\n<br>\n<br>\n\n<button name=\"searchPostButton\" button=\"submit\" (click)=\"filterByName()\" mat-raised-button color=\"primary\">Search</button>\n\n\n\n\n<mat-grid-list cols=\"2\" rowHeight=\"1:2\" >\n  <mat-grid-tile role=\"list\" *ngFor=\"let article of filteredArticles \" >\n\n    <mat-card class=\"container\">\n      <mat-card-title class=\"block-heading cleafix\">{{article.author}} </mat-card-title>\n      <mat-card-subtitle>  at   {{article.timeStamp}}</mat-card-subtitle>\n      <img  class=\"block-body\" mat-card-image src=\"{{article.image}}\"  class=\"card-image\" style=\"height: 50%\">\n      <mat-card-content>{{article.text}}</mat-card-content>\n      <mat-card-actions>\n        <button type=\"button\" class=\"stop_button\">Edit</button>\n      </mat-card-actions>\n      <mat-card-actions>\n        <button type=\"button\" class=\"stop_button\">Comment</button>\n      </mat-card-actions>\n    </mat-card>\n  </mat-grid-tile>\n</mat-grid-list>\n"

/***/ }),

/***/ "./src/app/main/posts/posts.component.ts":
/*!***********************************************!*\
  !*** ./src/app/main/posts/posts.component.ts ***!
  \***********************************************/
/*! exports provided: PostsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostsComponent", function() { return PostsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _json_data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../json-data.service */ "./src/app/json-data.service.ts");
/* harmony import */ var _node_modules_ngx_logger__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/ngx-logger */ "./node_modules/ngx-logger/esm5/ngx-logger.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var PostsComponent = /** @class */ (function () {
    function PostsComponent(getDataService, logger) {
        var _this = this;
        this.getDataService = getDataService;
        this.logger = logger;
        this.articles = [];
        this.filteredArticles = [];
        this.followedUserIdList = [];
        this.allUsers = [];
        //// console.log(this.searchText);
        this.articles = this.getDataService.getAllPosts();
        // this.getDataService.getAllPosts().then((articles: Post[]) => {this.articles = articles;  });
        this.followedUserIdList = this.getDataService.getFollowedUsersIdList();
        this.logger.debug('[posts.component.ts : PostsComponent.constructor()] ' + 'in constructor');
        this.allUsers = this.getDataService.getAllUsers();
        this.getDataService.dataUpdated().subscribe(function () {
            _this.filteredArticles = JSON.parse(JSON.stringify(_this.articles));
            _this.logger.debug('[posts.component.ts : PostsComponent.subscribe()] '
                + 'before filter by name, fArti= ' + _this.filteredArticles.length);
            _this.filterByName();
            _this.logger.debug('[posts.component.ts : PostsComponent.subscribe()] '
                + 'after filter by name, fArti= ' + _this.filteredArticles.length);
            _this.filterByAuthor();
            _this.logger.debug('[posts.component.ts : PostsComponent.subscribe()] '
                + 'after filter by author, fArti= ' + _this.filteredArticles.length);
        });
    }
    PostsComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                // await this.getDataService.getAllPostsObservable().then((allPostInObservable: Post[]) => {
                //   this.articles = allPostInObservable;
                //   this.logger.debug('[posts.component.ts : PostsComponent.()] ' + 'this.articles.length' + this.articles.length);
                // });
                //// console.log(this.articles);
                this.filteredArticles = this.articles;
                this.filterByName();
                this.filterByAuthor();
                this.logger.debug('[posts.component.ts : PostsComponent.ngOnInit()] ' + 'in ngOnInit');
                this.logger.debug('[posts.component.ts : PostsComponent.ngOnInit()] ' + 'this.articles= ' + this.articles.length);
                return [2 /*return*/];
            });
        });
    };
    PostsComponent.prototype.filterByName = function () {
        var _this = this;
        //// console.log(this.articles);
        if (this.searchText !== '' && this.articles !== undefined && this.searchText !== undefined) {
            this.filteredArticles = [];
            this.articles.forEach(function (element) {
                var isInAuthor = element.author.toUpperCase().indexOf(_this.searchText.toUpperCase()) >= 0;
                var isInText = element.text.toUpperCase().indexOf(_this.searchText.toUpperCase()) >= 0;
                if (isInAuthor || isInText) {
                    _this.filteredArticles.push(element);
                }
            });
        }
        else {
            // this.filteredArticles = this.articles;
        }
        //// console.log(this.filteredArticles);
        // this.ngOnInit();
        // window.location.reload();
    };
    PostsComponent.prototype.filterByAuthor = function () {
        this.logger.debug('[posts.component.ts : PostsComponent.filterByAuthor()] ');
        var i = 0;
        while (i < this.filteredArticles.length) {
            var isInAuthor = this.compareAuthArray(this.filteredArticles[i]);
            if (!isInAuthor) {
                this.filteredArticles.splice(i, 1);
            }
            else {
                i += 1;
            }
        }
    };
    PostsComponent.prototype.compareAuthArray = function (post) {
        var _this = this;
        this.logger.debug('[posts.component.ts : PostsComponent.compareAuthArray()] ' + 'IdList' + this.followedUserIdList);
        this.logger.debug('[posts.component.ts : PostsComponent.compareAuthArray()] ' + 'allUser' + this.allUsers);
        var isInAuthor = false;
        this.followedUserIdList.forEach(function (id) {
            var username = '';
            _this.logger.debug('[posts.component.ts : PostsComponent.()] ' + 'this.allUsers: ' + _this.allUsers);
            _this.allUsers.forEach(function (user) {
                if (user.id === id) {
                    username = user.username;
                }
            });
            _this.logger.debug('[posts.component.ts : PostsComponent.()] ' + 'username' + username);
            if (post.author === username) {
                isInAuthor = true;
                return true;
            }
        });
        return isInAuthor;
    };
    PostsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-posts',
            template: __webpack_require__(/*! ./posts.component.html */ "./src/app/main/posts/posts.component.html"),
            styles: [__webpack_require__(/*! ./posts.component.css */ "./src/app/main/posts/posts.component.css")]
        }),
        __metadata("design:paramtypes", [_json_data_service__WEBPACK_IMPORTED_MODULE_1__["JsonDataService"], _node_modules_ngx_logger__WEBPACK_IMPORTED_MODULE_2__["NGXLogger"]])
    ], PostsComponent);
    return PostsComponent;
}());



/***/ }),

/***/ "./src/app/profile/current-info/current-info.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/profile/current-info/current-info.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "div {\n  /*margin:0px auto;*/\n  /*width:300px;*/\n  height:50px;\n  /*background-color:red;*/\n  text-align:center;\n  /*font-size:32px;*/\n  /*color:white;*/\n}\n"

/***/ }),

/***/ "./src/app/profile/current-info/current-info.component.html":
/*!******************************************************************!*\
  !*** ./src/app/profile/current-info/current-info.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  <div class=\"current-username\">Username: {{loginUser.username}}</div>\n  <div class=\"current-email\">Email: {{loginUser.email}}</div>\n  <div class=\"current-phone\">Phone: {{loginUser.phone}}</div>\n  <div>Birthday: {{loginUser.birthday}}</div>\n  <div>Zipcode: {{loginUser.zipcode}}</div>\n</div>\n"

/***/ }),

/***/ "./src/app/profile/current-info/current-info.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/profile/current-info/current-info.component.ts ***!
  \****************************************************************/
/*! exports provided: CurrentInfoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurrentInfoComponent", function() { return CurrentInfoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _user__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../user */ "./src/app/user.ts");
/* harmony import */ var _json_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../json-data.service */ "./src/app/json-data.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CurrentInfoComponent = /** @class */ (function () {
    function CurrentInfoComponent(getDataService) {
        this.getDataService = getDataService;
        this.loginUser = new _user__WEBPACK_IMPORTED_MODULE_1__["User"]();
        // console.log('in current-info.component loginUser= ' + this.loginUser);
    }
    CurrentInfoComponent.prototype.ngOnInit = function () {
        // this.loginUser = this.getDataService.getLoginUser();
        // this.loginUser = localStorage['loginUser'];
        //// console.log(localStorage['testInfo']);
        //// console.log('in profile current-info ngOnInit ');
        //// console.log(this.loginUser.username);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _user__WEBPACK_IMPORTED_MODULE_1__["User"])
    ], CurrentInfoComponent.prototype, "loginUser", void 0);
    CurrentInfoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-current-info',
            template: __webpack_require__(/*! ./current-info.component.html */ "./src/app/profile/current-info/current-info.component.html"),
            styles: [__webpack_require__(/*! ./current-info.component.css */ "./src/app/profile/current-info/current-info.component.css")]
        }),
        __metadata("design:paramtypes", [_json_data_service__WEBPACK_IMPORTED_MODULE_2__["JsonDataService"]])
    ], CurrentInfoComponent);
    return CurrentInfoComponent;
}());



/***/ }),

/***/ "./src/app/profile/profile.component.css":
/*!***********************************************!*\
  !*** ./src/app/profile/profile.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/profile/profile.component.html":
/*!************************************************!*\
  !*** ./src/app/profile/profile.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<!DOCTYPE html>-->\n<!--<html lang=\"en\">-->\n<!--<head>-->\n<!--<title>Bootstrap Example</title>-->\n<!--<meta charset=\"utf-8\">-->\n<!--<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">-->\n<!--<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\">-->\n<!--<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>-->\n<!--<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js\"></script>-->\n<!--<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js\"></script>-->\n<!--</head>-->\n<!--<body>-->\n\n\n<!--<div class=\"container-fluid\">-->\n<!--<div class=\"container-fluid\">-->\n\n<!--<div class=\"row\">-->\n<!---->\n<!--<div class=\"col\"  ><app-current-info></app-current-info></div>-->\n<!--<div class=\"col\" ><app-update-info></app-update-info></div>-->\n<!--</div>-->\n<!--</div>-->\n<!--</div>-->\n<div class=\"demo-panel  \">\n\n\n  <mat-grid-list cols=\"4\">\n    <mat-grid-tile [colspan]=\"2\">\n\n          <div style=\"display: block\"><a routerLink=\"/main\">Main Page</a></div>\n\n          <div style=\"display: block\"><img\n            src=\"https://smallbusiness.fedex.com/content/dam/SMB/SMAC/Wide/0002_Talking%20Small-assets-C.jpg\"\n            class=\"card-image img-responsive\" style=\"max-width: 100px; max-height: 100px; width:100%;height: 100%\">\n          </div>\n\n          <div><input type=\"file\" id=\"mapSelector\" value=\"Upload new picture\"></div>\n    </mat-grid-tile>\n\n    <mat-grid-tile [colspan]=\"2\">\n      <img src=\"https://s1.ax1x.com/2018/10/06/i8bEkD.png\" style=\"width: 100%\">\n    </mat-grid-tile>\n\n\n    <mat-grid-tile [colspan]=\"2\" [rowspan]=\"2\">\n      <div class=\"col\">\n        <app-current-info [loginUser]=\"loginUser\"></app-current-info>\n      </div>\n    </mat-grid-tile>\n\n\n    <mat-grid-tile [colspan]=\"2\" [rowspan]=\"2\">\n      <div class=\"col\">\n        <app-update-info [loginUser]=\"loginUser\"></app-update-info>\n      </div>\n    </mat-grid-tile>\n  </mat-grid-list>\n\n</div>\n<!--</body>-->\n<!--</html>-->\n"

/***/ }),

/***/ "./src/app/profile/profile.component.ts":
/*!**********************************************!*\
  !*** ./src/app/profile/profile.component.ts ***!
  \**********************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _user__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../user */ "./src/app/user.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _json_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../json-data.service */ "./src/app/json-data.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(router, getDataService) {
        var _this = this;
        this.router = router;
        this.getDataService = getDataService;
        this.loginUser = new _user__WEBPACK_IMPORTED_MODULE_1__["User"]();
        this.getDataService.getLoginPromise().subscribe(function (jsonUser) {
            _this.loginUser = jsonUser;
        });
    }
    ProfileComponent.prototype.ngOnInit = function () {
    };
    ProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__(/*! ./profile.component.html */ "./src/app/profile/profile.component.html"),
            styles: [__webpack_require__(/*! ./profile.component.css */ "./src/app/profile/profile.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _json_data_service__WEBPACK_IMPORTED_MODULE_3__["JsonDataService"]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "./src/app/profile/update-info/update-info.component.css":
/*!***************************************************************!*\
  !*** ./src/app/profile/update-info/update-info.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "div{\n  /*margin:0px auto;*/\n  /*width:300px;*/\n  height:50px;\n  /*background-color:red;*/\n  text-align:center;\n  /*font-size:32px;*/\n  /*color:white;*/\n}\n"

/***/ }),

/***/ "./src/app/profile/update-info/update-info.component.html":
/*!****************************************************************!*\
  !*** ./src/app/profile/update-info/update-info.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n<form  #logForm=\"ngForm\" (ngSubmit)=\"onSubmit(logForm)\" novalidate >\n  <label>user name*:</label>\n  <input ngModel name=\"username\" required   #UsernameValue=\"ngModel\"  pattern=\"^[a-zA-Z][a-zA-Z0-9]*$\"><br>\n  <div class=\"alert\" [hidden]=\" (UsernameValue.valid && UsernameValue.value!=='' ) || UsernameValue.pristine\" >\n    Please only use letter and number as username and start with letter\n  </div>\n  <br>\n\n\n  <label>email address*:</label>\n  <input ngModel name=\"email\" required placeholder=\"a@b.com\" #EmailValue=\"ngModel\"  pattern=\"^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$\"><br>\n  <div class=\"alert\" [hidden]=\"EmailValue.valid || EmailValue.pristine\">\n    Please enter valid email\n  </div>\n  <br>\n\n\n\n\n  <label>phone number*:</label>\n  <input ngModel name=\"phone\" required placeholder=\"123-123-1234\" #PhoneValue=\"ngModel\"  pattern=\"^\\d{3}-\\d{3}-\\d{4}$\"><br>\n  <div class=\"alert\" [hidden]=\"PhoneValue.valid || PhoneValue.pristine\">\n    Please enter valid phone number\n  </div>\n  <br>\n\n\n\n\n\n\n  <label>zipcode*:</label>\n  <input ngModel name=\"zipcode\" required  type=\"text\" placeholder=\"00000\"  #ZipcodeValue=\"ngModel\"  pattern=\"^\\d{5}$\"   ><br>\n  <div class=\"alert\" [hidden]=\"ZipcodeValue.valid || ZipcodeValue.pristine\">\n    Please enter valid zipcode\n  </div>\n  <br>\n\n\n\n\n\n  <label>password:</label>\n  <input ngModel name=\"password\" type=\"password\"required  #PasswordValue=\"ngModel\"><br>\n\n  <button  [disabled]=\"!logForm.valid && !logForm.pristine\">Submit</button>\n  <!--<button >Submit</button>-->\n  <!--<button (click)=\"onSubmit()\">Submit</button>-->\n</form>\n</div>\n"

/***/ }),

/***/ "./src/app/profile/update-info/update-info.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/profile/update-info/update-info.component.ts ***!
  \**************************************************************/
/*! exports provided: UpdateInfoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateInfoComponent", function() { return UpdateInfoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _user__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../user */ "./src/app/user.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _json_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../json-data.service */ "./src/app/json-data.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UpdateInfoComponent = /** @class */ (function () {
    function UpdateInfoComponent(router, getDataService) {
        this.router = router;
        this.getDataService = getDataService;
        this.loginUser = new _user__WEBPACK_IMPORTED_MODULE_1__["User"]();
        // console.log('in update-info.component loginUser= ' + this.loginUser);
    }
    UpdateInfoComponent.prototype.ngOnInit = function () {
    };
    UpdateInfoComponent.prototype.onSubmit = function (form) {
        // console.log('form.value= ' + form.value);
        // console.log('form.valid= ' + form.valid);
        if (form.valid) {
            this.loginUser.username = form.value['username'];
            this.loginUser.email = form.value['email'];
            this.loginUser.phone = form.value['phone'];
            this.loginUser.zipcode = form.value['zipcode'];
            this.loginUser.password = form.value['password'];
            // console.log('form.value[\'username\']= ' + form.value['username']);
            // this.router.navigate(['main']);
            //// console.log('get input user title: ' + this.currentUser.username);
            //// console.log('get input user password: ' + this.currentUser.password);
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _user__WEBPACK_IMPORTED_MODULE_1__["User"])
    ], UpdateInfoComponent.prototype, "loginUser", void 0);
    UpdateInfoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-update-info',
            template: __webpack_require__(/*! ./update-info.component.html */ "./src/app/profile/update-info/update-info.component.html"),
            styles: [__webpack_require__(/*! ./update-info.component.css */ "./src/app/profile/update-info/update-info.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _json_data_service__WEBPACK_IMPORTED_MODULE_3__["JsonDataService"]])
    ], UpdateInfoComponent);
    return UpdateInfoComponent;
}());



/***/ }),

/***/ "./src/app/user.ts":
/*!*************************!*\
  !*** ./src/app/user.ts ***!
  \*************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var User = /** @class */ (function () {
    function User() {
        this.username = '';
        this.password = '';
        this.passwordConfirmation = '';
        this.zipcode = '';
        this.birthday = new Date();
        this.phone = '';
        this.email = '';
    }
    return User;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/zhexinxiao/Project/WebProject/COMP531_WEBAPP/aasignment/PA_5/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map