import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainComponent} from './main/main.component';
import {AuthComponent} from './auth/auth.component';
import {ProfileComponent} from './profile/profile.component';
import {LoginComponent} from './auth/login/login.component';
import {RegisterationComponent} from './auth/registeration/registeration.component';
import {UpdateInfoComponent} from './profile/update-info/update-info.component';
import {CurrentInfoComponent} from './profile/current-info/current-info.component';
import {FollowingComponent} from './main/following/following.component';

export const routes: Routes = [
  {path: '', redirectTo: '/auth', pathMatch: 'full'},
  {path: 'main', component: MainComponent},
  {path: 'auth', component: AuthComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'login', component: LoginComponent},
  {path: 'registration', component: RegisterationComponent},
  {path: 'updateInfo', component: UpdateInfoComponent},
  {path: 'currentInfo', component: CurrentInfoComponent},
  {path: 'following', component: FollowingComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
