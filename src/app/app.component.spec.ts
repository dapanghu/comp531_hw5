import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {MainComponent} from './main/main.component';
import {FollowingComponent} from './main/following/following.component';
import {AuthComponent} from './auth/auth.component';
import {LoginComponent} from './auth/login/login.component';
import {RegisterationComponent} from './auth/registeration/registeration.component';
import {HeadlineComponent} from './main/headline/headline.component';
import {PostsComponent} from './main/posts/posts.component';
import {ProfileComponent} from './profile/profile.component';
import {CurrentInfoComponent} from './profile/current-info/current-info.component';
import {UpdateInfoComponent} from './profile/update-info/update-info.component';
import {AppRoutingModule, routes} from './app-routing.module';
import {JsonDataService} from './json-data.service';
import {RouterTestingModule} from '../../node_modules/@angular/router/testing';
import {FormsModule} from '../../node_modules/@angular/forms';
import {BrowserModule} from '../../node_modules/@angular/platform-browser';
import {BrowserAnimationsModule} from '../../node_modules/@angular/platform-browser/animations';
import {MatMenuModule, MatButtonModule, MatIconModule, MatCardModule, MatToolbarModule, MatInputModule,
  MatFormFieldModule,
  MatSidenavModule,
  MatGridListModule} from '../../node_modules/@angular/material';
import {HttpClientModule} from '../../node_modules/@angular/common/http';
import {LoggerModule, NgxLoggerLevel, NGXLogger} from '../../node_modules/ngx-logger';
import {APP_BASE_HREF} from '../../node_modules/@angular/common';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MainComponent,
        FollowingComponent,
        AppComponent,
        AuthComponent,
        LoginComponent,
        RegisterationComponent,
        HeadlineComponent,
        PostsComponent,
        ProfileComponent,
        CurrentInfoComponent,
        UpdateInfoComponent],

      imports: [
        RouterTestingModule.withRoutes(routes),
        FormsModule,
        RouterTestingModule,
        FormsModule,
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatMenuModule,
        MatButtonModule,
        MatIconModule,
        MatCardModule,
        MatToolbarModule,
        MatFormFieldModule,
        MatInputModule,
        MatSidenavModule,
        MatGridListModule,
        HttpClientModule, LoggerModule.forRoot({
          serverLoggingUrl: '/api/logs',
          level: NgxLoggerLevel.DEBUG
        })
      ],
      providers: [JsonDataService, NGXLogger, {provide: APP_BASE_HREF, useValue: '/'}]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'RiceBook'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Rice Book');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to Rice Book!');
  }));
});
