import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {AuthComponent} from './auth/auth.component';
import {LoginComponent} from './auth/login/login.component';
import {RegisterationComponent} from './auth/registeration/registeration.component';
import {MainComponent} from './main/main.component';
import {FollowingComponent} from './main/following/following.component';
import {HeadlineComponent} from './main/headline/headline.component';
import {PostsComponent} from './main/posts/posts.component';
import {ProfileComponent} from './profile/profile.component';
import {AppRoutingModule} from './app-routing.module';
import {CurrentInfoComponent} from './profile/current-info/current-info.component';
import {UpdateInfoComponent} from './profile/update-info/update-info.component';
import {MatMenuModule, MatButtonModule, MatIconModule, MatCardModule, MatSidenavModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule} from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';

import {MatToolbarModule, MatFormFieldModule, MatInputModule, MatGridListModule} from '@angular/material';

import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    LoginComponent,
    RegisterationComponent,
    MainComponent,
    FollowingComponent,
    HeadlineComponent,
    PostsComponent,
    ProfileComponent,
    CurrentInfoComponent,
    UpdateInfoComponent

  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatSidenavModule,
    FlexLayoutModule,
    MatGridListModule,
    HttpClientModule,
    LoggerModule.forRoot({serverLoggingUrl: '/api/logs', level: NgxLoggerLevel.WARN})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
