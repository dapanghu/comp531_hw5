import {async, ComponentFixture, fakeAsync, inject, TestBed, tick} from '@angular/core/testing';

import {BrowserModule, By} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {DebugElement} from '@angular/core';
import {LoginComponent} from '../auth/login/login.component';
import {FormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {JsonDataService} from '../json-data.service';

import {NGXLogger} from 'ngx-logger';
import {LoggerModule, NgxLoggerLevel} from 'ngx-logger';
import {AppRoutingModule} from '../app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule, MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule, MatSidenavModule,
  MatToolbarModule
} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {AppComponent} from '../app.component';
import {AuthComponent} from '../auth/auth.component';
import {RegisterationComponent} from '../auth/registeration/registeration.component';
import {ProfileComponent} from '../profile/profile.component';
import {CurrentInfoComponent} from '../profile/current-info/current-info.component';
import {UpdateInfoComponent} from '../profile/update-info/update-info.component';
import {APP_BASE_HREF, Location} from '@angular/common';
import {routes} from '../app-routing.module';

describe('AuthComponent', () => {
  let component: AuthComponent;
  let fixture: ComponentFixture<AuthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        AuthComponent,
        LoginComponent,
        RegisterationComponent,
        ProfileComponent,
        CurrentInfoComponent,
        UpdateInfoComponent],

      imports: [
        RouterTestingModule.withRoutes(routes),
        FormsModule,
        RouterTestingModule,
        FormsModule,
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatMenuModule,
        MatButtonModule,
        MatIconModule,
        MatCardModule,
        MatToolbarModule,
        MatFormFieldModule,
        MatInputModule,
        MatSidenavModule,
        FlexLayoutModule,
        MatGridListModule,
        HttpClientModule, LoggerModule.forRoot({
          serverLoggingUrl: '/api/logs',
          level: NgxLoggerLevel.DEBUG
        })
      ],
      providers: [JsonDataService, NGXLogger, {provide: APP_BASE_HREF, useValue: '/'}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  // Registered user can log in with netID and password,
  // if successful then redirect to the Main view,
  // otherwise inform user of incorrect login
  it('should log in with hardcoded netID and user', () => {
    const username = 'zx17';
    const password = 'Test';

    expect(component).toBeTruthy();
  });
});
