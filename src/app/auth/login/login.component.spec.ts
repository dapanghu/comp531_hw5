import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {LoginComponent} from './login.component';
import {formGroupNameProvider} from '@angular/forms/src/directives/reactive_directives/form_group_name';
import {NgForm, FormGroup, FormsModule} from '@angular/forms';
import {DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';
import {JsonDataService} from '../../json-data.service';
import {Router} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {NGXLogger} from 'ngx-logger';
import {LoggerModule, NgxLoggerLevel} from 'ngx-logger';
import {APP_BASE_HREF} from '@angular/common';

// import {routes} from '../../app-routing.module';
// TODO
describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let usernameInput: DebugElement;
  let loginErrorMsg: DebugElement;
  let passwordInput: DebugElement;
  let submitButton: DebugElement;
  let loginForm: DebugElement;
  // let location: Location;
  let router: Router;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [FormsModule,
        RouterTestingModule,
        HttpClientModule, LoggerModule.forRoot({
          serverLoggingUrl: '/api/logs',
          level: NgxLoggerLevel.DEBUG
        })
      ],
      providers: [JsonDataService, NGXLogger, {provide: APP_BASE_HREF, useValue: '/'}]
    }).compileComponents();
    fixture = TestBed.createComponent(LoginComponent);
    fixture.detectChanges();
    component = fixture.componentInstance;


    usernameInput = fixture.debugElement.query(By.css('input[name="Username"]'));
    passwordInput = fixture.debugElement.query(By.css('input[name="Password"]'));
    loginErrorMsg = fixture.debugElement.query(By.css('.login_error'));
    submitButton = fixture.debugElement.query(By.css('button'));
    loginForm = fixture.debugElement.query(By.css('form'));
    router = fixture.debugElement.injector.get(Router);
    // router = TestBed.get(Router);
    // location = TestBed.get(Location);
    // router.initialNavigation();

    // fixture.whenStable()
    //   .then(() => {
    //     // update view
    //     fixture.detectChanges();
    //
    //
    //   });


  }));


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('checkLoginUser work correctly', () => {
    expect(component.checkLoginUser('zx17', 'Test')).toBeTruthy();
    expect(component.checkLoginUser('zx17', 'test')).toBeFalsy();
    expect(component.checkLoginUser('', '')).toBeFalsy();
  });


  it('ngSubmit work correctly when checkLoginUser return true',
    fakeAsync(() => {
      const spyRouter = spyOn(router, 'navigate');
      spyOn(component, 'checkLoginUser').and.returnValue(true);
      const spyOnSubmit = spyOn(component, 'onSubmit').and.callThrough();
      fixture.detectChanges();
      loginForm.triggerEventHandler('ngSubmit', null);

      expect(spyOnSubmit).toHaveBeenCalled();

      tick();
      expect(spyRouter.calls.argsFor(0)[0]).toEqual(['main']);

    }));

  it('ngSubmit work correctly when checkLoginUser return false ',
    fakeAsync(() => {
      const spyRouter = spyOn(router, 'navigate');
      spyOn(component, 'checkLoginUser').and.returnValue(false);
      const spyOnSubmit = spyOn(component, 'onSubmit').and.callThrough();
      fixture.detectChanges();
      loginForm.triggerEventHandler('ngSubmit', null);

      expect(spyOnSubmit).toHaveBeenCalled();

      tick();
      expect(spyRouter.calls.any()).toEqual(false);

    }));

  it('should log in a previously registered user, both checkLoginUser and onsubmit work correctly',
    fakeAsync(() => {
      usernameInput.nativeElement.value = 'zx17';
      usernameInput.nativeElement.dispatchEvent(new Event('input'));

      passwordInput.nativeElement.value = 'Test';
      passwordInput.nativeElement.dispatchEvent(new Event('input'));
      // console.log('[login.component.spec.ts : .()] ' + 'usernameInput is ' + usernameInput.nativeElement.name);
      // console.log('[login.component.spec.ts : .()] ' + 'submitButton is ' + submitButton.nativeElement.name);


      const spyRouter = spyOn(router, 'navigate');


      const spyOnSubmit = spyOn(component, 'onSubmit').and.callThrough();
      fixture.detectChanges();
      loginForm.triggerEventHandler('ngSubmit', null);

      expect(spyOnSubmit).toHaveBeenCalled();

      tick();
      // expect(location.pathname).toBe('main');
      // console.log( spyRouter.calls.first().args[0][0] );
      // .args[0];
      expect(spyRouter.calls.first().args[0][0]).toEqual('main');

    }));

  it('should not log in an invalid user',
    fakeAsync(() => {
      usernameInput.nativeElement.value = 'test';
      usernameInput.nativeElement.dispatchEvent(new Event('input'));

      passwordInput.nativeElement.value = '';
      passwordInput.nativeElement.dispatchEvent(new Event('input'));
      // console.log('[login.component.spec.ts : .()] ' + 'usernameInput is ' + usernameInput.nativeElement.name);
      // console.log('[login.component.spec.ts : .()] ' + 'submitButton is ' + submitButton.nativeElement.name);


      const spyRouter = spyOn(router, 'navigate');


      const spyOnSubmit = spyOn(component, 'onSubmit').and.callThrough();
      fixture.detectChanges();
      loginForm.triggerEventHandler('ngSubmit', null);

      expect(spyOnSubmit).toHaveBeenCalled();

      tick();
      // expect(location.pathname).toBe('main');
      // console.log( spyRouter.calls.first().args[0][0] );
      // .args[0];
      expect(spyRouter).not.toHaveBeenCalled();

    }));

  it('should update error message',
    fakeAsync(() => {
      usernameInput.nativeElement.value = 'test';
      usernameInput.nativeElement.dispatchEvent(new Event('input'));

      passwordInput.nativeElement.value = '';
      passwordInput.nativeElement.dispatchEvent(new Event('input'));

      // console.log('[login.component.spec.ts : .()] ' + 'usernameInput is ' + usernameInput.nativeElement.name);
      // console.log('[login.component.spec.ts : .()] ' + 'submitButton is ' + submitButton.nativeElement.name);


      fixture.detectChanges();
      loginForm.triggerEventHandler('ngSubmit', null);


      fixture.detectChanges();
      expect(loginErrorMsg.nativeElement.hidden = false);

      // expect(location.pathname).toBe('main');
      // console.log( spyRouter.calls.first().args[0][0] );
      // .args[0];

    }));

  it('should update success message ',
    fakeAsync(() => {
      usernameInput.nativeElement.value = 'zx17';
      usernameInput.nativeElement.dispatchEvent(new Event('input'));

      passwordInput.nativeElement.value = 'Test';
      passwordInput.nativeElement.dispatchEvent(new Event('input'));

      // console.log('[login.component.spec.ts : .()] ' + 'usernameInput is ' + usernameInput.nativeElement.name);
      // console.log('[login.component.spec.ts : .()] ' + 'submitButton is ' + submitButton.nativeElement.name);


      fixture.detectChanges();
      loginForm.triggerEventHandler('ngSubmit', null);


      fixture.detectChanges();
      expect(loginErrorMsg.nativeElement.hidden = true);

      // expect(location.pathname).toBe('main');
      // console.log( spyRouter.calls.first().args[0][0] );
      // .args[0];

    }));
});
