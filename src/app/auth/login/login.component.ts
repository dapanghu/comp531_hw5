import {Component, OnInit} from '@angular/core';
import {User} from '../../user';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {JsonDataService} from '../../json-data.service';
import {NGXLogger} from 'ngx-logger';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginError = false;
  loginUser: User = new User();


  constructor(private getDataService: JsonDataService, private router: Router, private logger: NGXLogger) {
    this.getDataService.getLoginPromise().subscribe((jsonUser: User) => {
      this.loginUser = jsonUser;
    });
    this.logger.debug('[login.component.ts : LoginComponent.constructor()] ' + 'in constructor');


  }

  checkLoginUser(username, password) {

    this.logger.debug('[login.component.ts : LoginComponent.checkLoginUser()]' + ' username: ' + username);
    this.logger.debug('[login.component.ts : LoginComponent.checkLoginUser()]' + ' password: ' + password);
    this.logger.debug('[login.component.ts : LoginComponent.checkLoginUser()]' + ' this.loginUser: ' + JSON.stringify(this.loginUser));

    // console.log('username: ' + username);

    // console.log('password' + password);

    if (username === this.loginUser.username && password === this.loginUser.password) {
      this.logger.debug('[login.component.ts : LoginComponent.checkLoginUser()]' + ' correct login user ');

      return true;
    }
    this.logger.debug('[login.component.ts : LoginComponent.checkLoginUser()]' + ' error login user ');
    return false;
  }


  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    // if (form.valid) {
    // console.log(form.value);
    // localStorage.setItem('loginUser', form.value);
    // localStorage.setItem('testInfo', 'testInfoxxx');
    this.logger.debug('[login.component.ts : LoginComponent.onSubmit()] ' + 'in onSubmit()');

    if (this.checkLoginUser(form.value['Username'], form.value['Password'])) {
      this.router.navigate(['main']).then(
        suc => {
          this.logger.debug('[login.component.ts : LoginComponent.()]' + 'redirect successful' + suc);
          this.loginError = false;
          return true;

        }, fail => {
          this.logger.debug('[login.component.ts : LoginComponent.()]' + 'can\'t redirect' + fail);
          return false;
        });
    } else {
      this.loginError = true;
    }

  }

// }
//// console.log('get input user title: ' + this.currentUser.username);
//// console.log('get input user password: ' + this.currentUser.password);
}


