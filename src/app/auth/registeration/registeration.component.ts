import {Component, OnInit} from '@angular/core';
import {User} from '../../user';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {JsonDataService} from '../../json-data.service';
import {NGXLogger} from 'ngx-logger';

@Component({
  selector: 'app-registeration',
  templateUrl: './registeration.component.html',
  styleUrls: ['./registeration.component.css']
})
export class RegisterationComponent implements OnInit {

  currentUser: User;

  constructor(private router: Router, private jsonDataService: JsonDataService, private logger: NGXLogger) {
  }

  ngOnInit() {
  }


  onSubmit(form: NgForm) {
    // if (form.valid) {
    // console.log(form.value);
    // localStorage.setItem('loginUser', form.value);
    // localStorage.setItem('testInfo', 'testInfoxxx');
    if (form.valid) {
      this.router.navigate(['/main']).then(
        suc => {
          this.logger.debug('[login.component.ts : LoginComponent.()]' + suc);

        }, fail => {
          this.logger.debug('[login.component.ts : LoginComponent.()]' + fail);
        });
    }

  }


}

