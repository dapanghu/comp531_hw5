import {Injectable, EventEmitter} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from './user';
import {forEach} from '@angular/router/src/utils/collection';
import {Post} from './main/post';
import {NGXLogger} from 'ngx-logger';
import {merge, Subscription, Subject, Observable} from 'rxjs';

// import 'rxjs/add/operator/mergeMap';
// import {flatMap, mergeMap} from 'rxjs/operators';


@Injectable({
  providedIn: 'root',
})
export class JsonDataService {
  allUsers: User[] = [];
  followedUsersIdList: number[] = [11, 12, 13];
  allPosts: Post[] = [];
  private subject = new Subject<any>();

  loginUser: User = undefined;

  // dataUpdated: EventEmitter = new EventEmitter();

  constructor(private http: HttpClient, private logger: NGXLogger) {

    // console.log('in getAllUsers ');
    this.http.get('./assets/profile.json').subscribe(
      (jsonUser: User) => {

        // console.log('jsonUser= ' + jsonUser);
        this.loginUser = jsonUser;
        this.logger.debug('[json-data.service.ts : JsonDataService.()] ' + 'this.loginUser: ' + this.loginUser);

        // console.log(this.loginUser['username']);
        // console.log(this.loginUser['birthday']);
        // console.log(this.loginUser['phone']);
        // console.log(this.loginUser['phone']);
        // console.log(this.loginUser['email']);

      });


    //// console.log('in getAllUsers ');
    this.http.get('./assets/users.json').subscribe(
      (usersWithString: TempUser[]) => {
        //// console.log(usersWithString);
        usersWithString.forEach(userWithString => {
          const tempUser: User = {
            id: parseInt(userWithString['id'], 10),
            username: userWithString['name'],
            status: userWithString['status'],
            image: userWithString['image'],
            // todo change to real password
            password: '',
            passwordConfirmation: '',
            zipcode: '00000',
            birthday: new Date(),
            phone: '111-111-1111',
            email: 'a@b.com'

          };
          //// console.log(tempUser);
          this.allUsers.push(tempUser);
        });
        //// console.log(this.allUsers);
      });

    // this.http.get('./assets/articles.json').subscribe(
    //   (postsWithString: TempPost[]) => {
    //     //// console.log(usersWithString);
    //     postsWithString.forEach(postWithString => {
    //       const tempPost: Post = {
    //         id: parseInt(postWithString['id'], 10),
    //         title: postWithString['title'],
    //         author: postWithString['author'],
    //         text: postWithString['text'],
    //         image: postWithString['image'],
    //         timeStamp: postWithString['timeStamp']
    //       };
    //       //// console.log(tempUser);
    //       this.allPosts.push(tempPost);
    //       this.logger.debug('[json-data.service.ts : JsonDataService.()] ' + 'this.allPosts: ' + this.allPosts.length);
    //
    //     });
    //     // return this.allPosts;
    //     //// console.log(this.allUsers);
    //   });


  }

  updateData() {
    this.logger.debug('[json-data.service.ts : JsonDataService.sendMessage()] ' + 'send message to service');

    this.subject.next();
  }

  dataUpdated(): Observable<any> {
    // this.logger.debug('[json-data.service.ts : JsonDataService.dataUpdated()] '+'notify service date');

    return this.subject.asObservable();
  }

  getAllUsers() {
    this.logger.debug('[json-data.service.ts : JsonDataService.getAllUsers()] ' + 'this.allUsers: ' + this.allUsers.length);

    return this.allUsers;
  }

  // getAllPosts() {
  //   this.logger.debug('[json-data.service.ts : JsonDataService.getAllPosts()] ' + 'this.allPosts: ' + this.allPosts.length);
  //
  //   return this.allPosts;
  // }

  getLoginUser() {
    this.logger.debug('[json-data.service.ts : JsonDataService.getAllPosts()] ' + 'this.loginUser: ' + this.loginUser);

    return this.loginUser;
  }

  //
  // async getAllPosts() {
  //   //// console.log('in getAllUsers ');
  //   const response = <TempPost[]>await this.http.get('./assets/articles.json').toPromise();
  //
  //   response.forEach(postWithString => {
  //     const tempPost: Post = {
  //       id: parseInt(postWithString['id'], 10),
  //       title: postWithString['title'],
  //       author: postWithString['author'],
  //       text: postWithString['text'],
  //       image: postWithString['image'],
  //       timeStamp: postWithString['timeStamp']
  //     };
  //     //// console.log(tempUser);
  //     this.allPosts.push(tempPost);
  //     this.logger.debug('[json-data.service.ts : JsonDataService.getAllPosts()] ' + 'this.allPosts: ' + this.allPosts.length);
  //
  //   });
  //
  //
  //   return new Promise(function (resolve, reject) {
  //     resolve(this.allPosts);
  //   });
  //   //// console.log(this.allUsers);
  //
  //
  // }


  getAllPostsObservable() {
    return this.http.get('./assets/articles.json').toPromise().then(
      (postsWithString: TempPost[]) => {

        const tempPosts: Post[] = [];
        //// console.log(usersWithString);
        postsWithString.forEach(postWithString => {
          const tempPost: Post = {
            id: parseInt(postWithString['id'], 10),
            title: postWithString['title'],
            author: postWithString['author'],
            text: postWithString['text'],
            image: postWithString['image'],
            timeStamp: postWithString['timeStamp']
          };
          //// console.log(tempUser);
          tempPosts.push(tempPost);
          this.allPosts.push(tempPost);
          this.logger.debug('[json-data.service.ts : JsonDataService.getAllPosts()] ' + 'tempPosts: ' + tempPosts);

        });
        return this.allPosts;
        //// console.log(this.allUsers);

      });
  }


  getLoginUserObservable() {
    return this.http.get('./assets/profile.json').toPromise().then(
      (jsonUser: User) => {
        if (this.loginUser !== undefined) {
          return this.loginUser;
        }
        // console.log('jsonUser= ' + jsonUser);
        this.loginUser = jsonUser;
        this.logger.debug('[json-data.service.ts : JsonDataService.()] ' + 'this.loginUser: ' + this.loginUser);
        return this.loginUser;
      });
  }

  getFollowedUsersIdList() {
    return this.followedUsersIdList;
  }


  getAllUsersObservable() {
    return this.http.get('./assets/articles.json').toPromise().then(
      (usersWithString: TempUser[]) => {
        const tempUsers: User[] = [];

        //// console.log(usersWithString);
        usersWithString.forEach(userWithString => {
          const tempUser: User = {
            id: parseInt(userWithString['id'], 10),
            username: userWithString['name'],
            status: userWithString['status'],
            image: userWithString['image'],
            // todo change to real password
            password: '',
            passwordConfirmation: '',
            zipcode: '00000',
            birthday: new Date(),
            phone: '111-111-1111',
            email: 'a@b.com'

          };
          //// console.log(tempUser);
          this.allUsers.push(tempUser);
          tempUsers.push(tempUser);
        });
        return this.allUsers;
      });
  }


  getLoginPromise() {
    return this.http.get('./assets/profile.json');
  }

  putLoginUser(user: User) {
    //// console.log('in putLoginUser, user= ');
    // this.http.get('./assets/profile.json').subscribe(data =>// console.log('data= ' + JSON.stringify(data)));
    //// console.log(user);
    //// console.log(JSON.stringify(user));
    // this.http.post('./assets/profile.json', user).subscribe(
    //   data => {
    //    // console.log('PUT Request is successful ', data);
    //   },
    //   error => {
    //    // console.log('Rrror', error);
    //   }
    // );
  }


  id = 200000;

  addPost(postString: string) {


    if (postString !== '' && postString !== undefined) {
      const newPost: Post = {
          id: this.id,
          title: 'My post',
          // author: 'zx17',
          image: 'https://smallbusiness.fedex.com/content/dam/SMB/SMAC/Wide/0002_Talking%20Small-assets-C.jpg',
          author: this.loginUser.username,
          text: postString,
          timeStamp: new Date()
        }
      ;

      this.id -= 1;
      this.allPosts.splice(0, 0, newPost);
      this.logger.warn('[headline.component.ts : HeadlineComponent.post()] ' + 'add post, now allpost.length= ' + this.allPosts.length);

      // @ts-ignore
      this.updateData();
    }
    //// console.log(this.allPosts);
  }

}


interface TempLoginUser {
  id: string;
  name: string;
  status: string;
  image: string;
}


interface TempUser {
  id: string;
  name: string;
  status: string;
  image: string;
}


interface TempPost {
  id: string;
  title: string;
  author: string;
  image: string;
  text: string;
}


