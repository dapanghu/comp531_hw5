import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FollowingComponent} from './following.component';
import {MainComponent} from '../main.component';
import {AppComponent} from '../../app.component';
import {AuthComponent} from '../../auth/auth.component';
import {LoginComponent} from '../../auth/login/login.component';
import {RegisterationComponent} from '../../auth/registeration/registeration.component';
import {HeadlineComponent} from '../headline/headline.component';
import {PostsComponent} from '../posts/posts.component';
import {ProfileComponent} from '../../profile/profile.component';
import {CurrentInfoComponent} from '../../profile/current-info/current-info.component';
import {UpdateInfoComponent} from '../../profile/update-info/update-info.component';
import {AppRoutingModule, routes} from '../../app-routing.module';
import {JsonDataService} from '../../json-data.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule, MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule, MatSidenavModule,
  MatToolbarModule
} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {HttpClientModule} from '@angular/common/http';
import {APP_BASE_HREF} from '@angular/common';
import {NGXLogger} from 'ngx-logger';
import {LoggerModule, NgxLoggerLevel} from 'ngx-logger';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule} from '@angular/forms';
import {BrowserModule, By} from '@angular/platform-browser';

describe('FollowingComponent', () => {
  let component: FollowingComponent;
  let fixture: ComponentFixture<FollowingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MainComponent,
        FollowingComponent,
        AppComponent,
        AuthComponent,
        LoginComponent,
        RegisterationComponent,
        HeadlineComponent,
        PostsComponent,
        ProfileComponent,
        CurrentInfoComponent,
        UpdateInfoComponent],

      imports: [
        RouterTestingModule.withRoutes(routes),
        FormsModule,
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatMenuModule,
        MatButtonModule,
        MatIconModule,
        MatCardModule,
        MatToolbarModule,
        MatFormFieldModule,
        MatInputModule,
        MatSidenavModule,
        FlexLayoutModule,
        MatGridListModule,
        HttpClientModule, LoggerModule.forRoot({
          serverLoggingUrl: '/api/logs',
          level: NgxLoggerLevel.DEBUG
        })
      ],
      providers: [JsonDataService, NGXLogger, {provide: APP_BASE_HREF, useValue: '/'}]
    }).compileComponents();
    console.log('in following component beforeEach1');

  }));

  beforeEach(async(() => {
    console.log('in following component beforeEach2');
    fixture = TestBed.createComponent(FollowingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  // it('should remove articles when removing a follower', () => {
  //   console.log(' in following component it2');
  //   fixture.detectChanges();
  //   // const unfollowButton = fixture.debugElement.queryAll(By.css('.unfollowButton'));
  //   // console.log(unfollowButton.length);
  //   // fixture.detectChanges();
  //   //
  //   // const searchButton = fixture.debugElement.query(By.css('button[name="searchPostButton"]')).nativeElement;
  //   // searchButton.click();
  //   // fixture.detectChanges();
  //   // expect(fixture.debugElement.queryAll(By.css('mat-grid-tile')).length).toBeLessThan(oldArticlesNumber);
  // });

});
