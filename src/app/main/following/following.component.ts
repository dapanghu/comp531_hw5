import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';

// import {followedUsersIdList} from '../allUsers';
import {User} from '../../user';
import {JsonDataService} from '../../json-data.service';
import {NGXLogger} from '../../../../node_modules/ngx-logger';

@Component({
  selector: 'app-following',
  templateUrl: './following.component.html',
  styleUrls: ['./following.component.css']
})
export class FollowingComponent implements OnInit {
  inputHeadline: string;
  @Input() userDisplayed: User;
  @Input() isLoggedUser: boolean;
  followedUsersList: number[];

  headlineValue: string;

  @Input()
  get headline() {
    return this.headlineValue;
  }

  set headline(val) {
    this.headlineValue = val;
    this.headlineChange.emit(this.inputHeadline);
  }

  @Output()
  headlineChange = new EventEmitter<string>();

  @Output() updateHeadlineEvent: EventEmitter<string> = new EventEmitter<string>();

  constructor(private getDataService: JsonDataService, private logger: NGXLogger) {
    this.followedUsersList = this.getDataService.getFollowedUsersIdList();
  }

  ngOnInit() {
    //// console.log(followedUsersIdList);
    //// console.log(this.userDisplayed);
  }

  unfollow() {
    this.followedUsersList.splice(this.followedUsersList.indexOf(this.userDisplayed.id), 1);
    this.logger.debug('[following.component.ts : FollowingComponent.unfollow()] ' + 'followed user list: ' + this.followedUsersList);
    this.getDataService.updateData();
    //// console.log('in unfollow, ' + followedUsersIdList);
  }

  updateHeadline() {
    //// console.log(this.inputHeadline);
    //// console.log(document.getElementById('userHeadline'));
    if (this.inputHeadline !== undefined) {
      document.getElementById('userHeadline').innerHTML = this.inputHeadline;
      // TODO
      this.logger.debug('[following.component.ts : FollowingComponent.updateHeadline()] ' + 'get headline: ' + this.inputHeadline);
      this.userDisplayed.status = this.inputHeadline;
      this.updateHeadlineEvent.emit(this.inputHeadline);
      this.logger.debug('[following.component.ts : FollowingComponent.updateHeadline()] ' + 'get headline: ' + this.inputHeadline);
      localStorage.setItem('headline', this.inputHeadline);

    }
  }

}
