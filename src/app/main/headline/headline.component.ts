import {Component, OnInit} from '@angular/core';
import {Post} from '../post';
import {JsonDataService} from '../../json-data.service';
import {currentPostIdList} from '../allPosts';
import {User} from '../../user';
import {NGXLogger} from '../../../../node_modules/ngx-logger';

@Component({
  selector: 'app-headline',
  templateUrl: './headline.component.html',
  styleUrls: ['./headline.component.css']
})
export class HeadlineComponent implements OnInit {


  currentPostIdList = currentPostIdList;


  loginUser: User = new User();


  constructor(private getDataService: JsonDataService, private logger: NGXLogger) {

    this.getDataService.getLoginUserObservable().then((jsonUser: User) => {
      this.loginUser = jsonUser;
    });
  }

  ngOnInit() {
    //// console.log('id= ' + this.id);

    //// console.log(this.allPosts);

  }

  cancel() {

    // if (this.id < 200000) {
    //   this.allPosts.splice(0, 1);
    //   this.id += 1;
    // }

    // @ts-ignore
    // const post = document.getElementById('textarea').value;
    //// console.log('post= ' + post);
    // @ts-ignore
    document.getElementById('textarea').value = '';
  }

  id = 200000;

  post() {

    // @ts-ignore
    const postString = document.getElementById('textarea').value;

    this.getDataService.addPost(postString);
    // @ts-ignore
    document.getElementById('textarea').value = '';

  }

  //// console.log(this.allPosts);


}
