import {async, ComponentFixture, fakeAsync, inject, TestBed, tick} from '@angular/core/testing';

import {MainComponent} from './main.component';
import {BrowserModule, By} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {DebugElement} from '@angular/core';
import {LoginComponent} from '../auth/login/login.component';
import {FormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {JsonDataService} from '../json-data.service';

import {NGXLogger} from 'ngx-logger';
import {LoggerModule, NgxLoggerLevel} from 'ngx-logger';
import {FollowingComponent} from './following/following.component';
import {AppRoutingModule} from '../app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule, MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule, MatSidenavModule,
  MatToolbarModule
} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {AppComponent} from '../app.component';
import {AuthComponent} from '../auth/auth.component';
import {RegisterationComponent} from '../auth/registeration/registeration.component';
import {HeadlineComponent} from './headline/headline.component';
import {PostsComponent} from './posts/posts.component';
import {ProfileComponent} from '../profile/profile.component';
import {CurrentInfoComponent} from '../profile/current-info/current-info.component';
import {UpdateInfoComponent} from '../profile/update-info/update-info.component';
import {APP_BASE_HREF, Location} from '@angular/common';
import {routes} from '../app-routing.module';

describe('MainComponent', () => {
  let component: MainComponent;
  let fixture: ComponentFixture<MainComponent>;
  let logoutButton: DebugElement;
  let location: Location;
  let router: Router;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [MainComponent,
        FollowingComponent,
        AppComponent,
        AuthComponent,
        LoginComponent,
        RegisterationComponent,
        HeadlineComponent,
        PostsComponent,
        ProfileComponent,
        CurrentInfoComponent,
        UpdateInfoComponent],

      imports: [
        RouterTestingModule.withRoutes(routes),
        FormsModule,
        RouterTestingModule,
        FormsModule,
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatMenuModule,
        MatButtonModule,
        MatIconModule,
        MatCardModule,
        MatToolbarModule,
        MatFormFieldModule,
        MatInputModule,
        MatSidenavModule,
        FlexLayoutModule,
        MatGridListModule,
        HttpClientModule, LoggerModule.forRoot({
          serverLoggingUrl: '/api/logs',
          level: NgxLoggerLevel.DEBUG
        })
      ],
      providers: [JsonDataService, NGXLogger, {provide: APP_BASE_HREF, useValue: '/'}]
    }).compileComponents();
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(MainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    logoutButton = fixture.debugElement.query(By.css('.logOut_Button'));
    router = fixture.debugElement.injector.get(Router);
    location = TestBed.get(Location);

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should log out a user ',
    // fakeAsync(inject([Router, Location], (router: Router, location: Location) => {
    fakeAsync(() => {
      // const spyRouter = spyOn(router, 'navigate');


      logoutButton.nativeElement.click();
      fixture.detectChanges();

      tick(50);

      console.log('[main.component.spec.ts : .()] ' + 'location.path ' + location.path(true));
      console.log('[main.component.spec.ts : .()] ' + 'router.url ' + router.url);

      expect(location.path()).toBe('/auth');

    }));

  it('should remove articles when removing a follower', () => {
    fixture.detectChanges();
    const oldArticlesNumber = fixture.debugElement.queryAll(By.css('mat-grid-tile')).length;
    const unfollowButton = fixture.debugElement.queryAll(By.css('button[name="unfollowButton"]'));
    unfollowButton[0].nativeElement.click();
    fixture.detectChanges();
    expect(component.followedUserIdList).not.toContain(11);
    expect(fixture.debugElement.queryAll(By.css('mat-grid-tile')).length).toBeLessThan(oldArticlesNumber);

  });

  it('should add articles when adding a follower', () => {
    fixture.detectChanges();
    const oldArticlesNumber = fixture.debugElement.queryAll(By.css('mat-grid-tile')).length;
    const unfollowButton = fixture.debugElement.queryAll(By.css('button[name="unfollowButton"]'));
    unfollowButton[0].nativeElement.click();
    fixture.detectChanges();
    const followerInput = fixture.debugElement.queryAll(By.css('input[name="followerInput"]'));
    followerInput[0].nativeElement.value = 'Alice';
    followerInput[0].nativeElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const addButton = fixture.debugElement.queryAll(By.css('button[name="addButton"]'));
    addButton[0].nativeElement.click();
    fixture.detectChanges();


    expect(fixture.debugElement.queryAll(By.css('mat-grid-tile')).length).toEqual(oldArticlesNumber);

  });


});
