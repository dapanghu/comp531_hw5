import {Component, OnInit} from '@angular/core';
import {MatMenuModule, MatButtonModule, MatIconModule, MatCardModule, MatSidenavModule} from '@angular/material';
// import {followedUsersIdList} from './allUsers';

import {User} from '../user';

import {JsonDataService} from '../json-data.service';
import {Router} from '@angular/router';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {NGXLogger} from '../../../node_modules/ngx-logger';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  // providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}]
})
export class MainComponent implements OnInit {
  inputFollowedName: string;
  showAddFollowMsg: boolean = false;

  allUsers: User[] = [];
  followedUserIdList: number[] = [];
  id = 0;

  addFollowerMsg: string = '';
  loginUser: User = new User();


  constructor(private getDataService: JsonDataService, private location: Location, private logger: NGXLogger) {

    //// console.log('in constructor, ' );
    //// console.log(this.allUsers);
    //// console.log(this.allUsers.length);
    //// console.log(this.id);
    this.logger.debug('[main.component.ts : MainComponent.constructor()] ' + 'constructor start');

  }

  logout() {
    localStorage.clear();
    this.logger.debug('[main.component.ts : MainComponent.logout()] ');

  }

  addFollowedUser() {
    let i = 0;
    for (i = 0; i < this.allUsers.length; i++) {
      if (this.allUsers[i].username == this.inputFollowedName) {
        let j = 0;
        for (j = 0; j < this.followedUserIdList.length; j++) {
          if (this.allUsers[i].id === this.followedUserIdList[j]) {
            // output already followed
            // this.showAddFollowMsg = true;
            this.addFollowerMsg = 'already followed this user';
            return;
          }
        }
        this.followedUserIdList.push(this.allUsers[i].id);
        this.getDataService.updateData();
        this.addFollowerMsg = '';
        return;

      }
    }
    //output no such user
    this.addFollowerMsg = 'no such user';


    // this.id = 11 + this.allUsers.length;
    //
    // if (this.inputFollowedName !== '' && this.inputFollowedName !== undefined) {
    //   const newFollowedUser: User = {
    //     id: this.id,
    //     username: this.inputFollowedName,
    //     password: '',
    //     passwordConfirmation: '',
    //     zipcode: '00000',
    //     birthday: new Date(),
    //     phone: '111-111-1111',
    //     email: 'a@b.com',
    //     image: 'http://www.clker.com/' +
    //       'cliparts/q/a/C/u/Z/R/red-basic-female-symbol-hi.png',
    //     status: 'hahaha!'
    //   };
    //   this.followedUserIdList.push(this.id);
    //   this.allUsers.push(newFollowedUser);
    //   this.getDataService.updateData();
    // }
    //// console.log('in addFollow, ' );
    //// console.log(this.allUsers);
    //// console.log('in addFollow, ' + this.followedUserIdList);

  }


  updateLoginUserHeadline(headline: string) {
    this.logger.debug('[main.component.ts : MainComponent.updateLoginuserHeadLine()] ' + 'loginuser: ' + this.loginUser);
    this.loginUser.status = headline;
    this.logger.debug('[main.component.ts : MainComponent.updateLoginuserHeadLine()] ' + 'loginuser.status: ' + this.loginUser.status);
    this.logger.debug('[main.component.ts : MainComponent.updateLoginuserHeadLine()] ' +
      'getLoginUser.status: ' + this.getDataService.getLoginUser().status);

  }

  async ngOnInit() {
    this.logger.debug('[main.component.ts : MainComponent.ngOnInit()] ' + 'ngOnInit start');

    // this.getDataService.getAllUsers();
    this.getDataService.getAllUsersObservable().then(tempUsers => {
      this.allUsers = tempUsers;
    });
    // this.loginUser = this.getDataService.getLoginUser();
    this.getDataService.getLoginUserObservable().then((jsonUser: User) => {
      this.loginUser = JSON.parse(JSON.stringify(jsonUser));
      const headline = localStorage.getItem('headline');
      if (headline !== '' && headline !== undefined && headline !== null) {
        this.loginUser.status = headline;
      }
      this.logger.debug('[main.component.ts : MainComponent.()] ' + 'headline ' + headline);
      this.logger.debug('[main.component.ts : MainComponent.()] ' + 'loginUser ' + this.loginUser.status);

    });
    // this.loginUser = this.getDataService.getLoginUser();
    this.followedUserIdList = this.getDataService.getFollowedUsersIdList();

    (this.id = 11 + this.allUsers.length);

    //   //// console.log(this.loginUser);
  }

}
