export class Post {
  timeStamp: Date;
  id: number;
  title: string;
  author: string;
  image: string;
  text: string;
}
