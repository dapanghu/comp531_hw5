import {async, fakeAsync, tick, ComponentFixture, TestBed} from '@angular/core/testing';

import {PostsComponent} from './posts.component';
import {MainComponent} from '../main.component';
import {FollowingComponent} from '../following/following.component';
import {AppComponent} from '../../app.component';
import {AuthComponent} from '../../auth/auth.component';
import {LoginComponent} from '../../auth/login/login.component';
import {RegisterationComponent} from '../../auth/registeration/registeration.component';
import {HeadlineComponent} from '../headline/headline.component';
import {ProfileComponent} from '../../profile/profile.component';
import {CurrentInfoComponent} from '../../profile/current-info/current-info.component';
import {UpdateInfoComponent} from '../../profile/update-info/update-info.component';
import {RouterTestingModule} from '@angular/router/testing';
import {AppRoutingModule, routes} from '../../app-routing.module';
import {FormsModule} from '@angular/forms';
import {BrowserModule, By} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule, MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule, MatSidenavModule,
  MatToolbarModule
} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {HttpClientModule} from '@angular/common/http';
import {JsonDataService} from '../../json-data.service';
import {APP_BASE_HREF} from '@angular/common';
import {NGXLogger} from 'ngx-logger';
import {LoggerModule, NgxLoggerLevel} from 'ngx-logger';

describe('PostsComponent', () => {
  let component: PostsComponent;
  // let followingComponent: FollowingComponent;
  let fixture: ComponentFixture<PostsComponent>;
  // let fixtureFollowing: ComponentFixture<FollowingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MainComponent,
        FollowingComponent,
        AppComponent,
        AuthComponent,
        LoginComponent,
        RegisterationComponent,
        HeadlineComponent,
        PostsComponent,
        ProfileComponent,
        CurrentInfoComponent,
        UpdateInfoComponent],

      imports: [
        RouterTestingModule.withRoutes(routes),
        FormsModule,
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatMenuModule,
        MatButtonModule,
        MatIconModule,
        MatCardModule,
        MatToolbarModule,
        MatFormFieldModule,
        MatInputModule,
        MatSidenavModule,
        FlexLayoutModule,
        MatGridListModule,
        HttpClientModule, LoggerModule.forRoot({
          serverLoggingUrl: '/api/logs',
          level: NgxLoggerLevel.DEBUG
        })
      ],
      providers: [JsonDataService, NGXLogger, {provide: APP_BASE_HREF, useValue: '/'}]
    }).compileComponents();
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(PostsComponent);
    // fixtureFollowing = TestBed.createComponent(FollowingComponent);
    component = fixture.componentInstance;
    // followingComponent = fixtureFollowing.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // TODO
  it('should fetch articles for current logged in user', fakeAsync(() => {
    fixture.detectChanges();
    tick();
    console.log('in fetch article test');
    expect(component.articles.length).toBeGreaterThanOrEqual(8);
  }));

  // TODO
  it('should render articles', () => {
    fixture.detectChanges();
    const articleTiles = fixture.debugElement.queryAll(By.css('mat-grid-tile'));
    expect(articleTiles.length).toEqual(8);
  });


  it('should update the search keyword', () => {
    const keyword = 'dd';

    const articlesSearchInput = fixture.debugElement.query(By.css('input')).nativeElement;

    // update inputs
    articlesSearchInput.value = keyword;
    articlesSearchInput.dispatchEvent(new Event('input'));

    fixture.detectChanges();

    expect(component.searchText).toEqual(keyword);
  });
  it('should filter displayed articles by the search keyword', () => {
    fixture.detectChanges();
    const articleTiles = fixture.debugElement.queryAll(By.css('mat-grid-tile'));
    const oldArticlesNumber = articleTiles.length;

    const keyword = 'dd';

    const articlesSearchInput = fixture.debugElement.query(By.css('input')).nativeElement;

    // update inputs
    articlesSearchInput.value = keyword;
    articlesSearchInput.dispatchEvent(new Event('input'));

    fixture.detectChanges();

    const searchButton = fixture.debugElement.query(By.css('button[name="searchPostButton"]')).nativeElement;
    searchButton.click();
    fixture.detectChanges();
    expect(fixture.debugElement.queryAll(By.css('mat-grid-tile')).length).toBeLessThan(oldArticlesNumber);
  });



});
