import {Component, Input, OnInit} from '@angular/core';
import {Post} from '../post';
import {JsonDataService} from '../../json-data.service';
import {NGXLogger} from '../../../../node_modules/ngx-logger';
import {User} from '../../user';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  articles: Post[] = [];
  filteredArticles: Post[] = [];
  searchText: String;
  followedUserIdList: number[] = [];
  allUsers: User[] = [];
  loginUser: User = undefined;

  constructor(private getDataService: JsonDataService, private logger: NGXLogger) {
    //// console.log(this.searchText);

    // this.getDataService.getAllPosts().then((articles: Post[]) => {this.articles = articles;  });
    this.followedUserIdList = this.getDataService.getFollowedUsersIdList();
    this.logger.debug('[posts.component.ts : PostsComponent.constructor()] ' + 'in constructor');
    this.allUsers = this.getDataService.getAllUsers();
    this.getDataService.getLoginUserObservable().then((loginUser: User) => {
      this.loginUser = loginUser;
    });

    this.getDataService.getAllPostsObservable().then((allPosts: Post[]) => {
      this.articles = allPosts;
      this.filteredArticles = JSON.parse(JSON.stringify(this.articles));
      this.logger.debug('[posts.component.ts : PostsComponent.initsubscribe()] '
        + 'before filter by name, fArtiL= ' + this.filteredArticles.length);
      this.filterByName();
      this.logger.debug('[posts.component.ts : PostsComponent.initsubscribe()] '
        + 'after filter by name, fArtiL= ' + this.filteredArticles.length);

      this.filterByAuthor();
      this.logger.debug('[posts.component.ts : PostsComponent.initsubscribe()] '
        + 'after filter by author, fArtiL= ' + this.filteredArticles.length);
    });


    this.getDataService.dataUpdated().subscribe(() => {
      this.filteredArticles = JSON.parse(JSON.stringify(this.articles));
      this.logger.debug('[posts.component.ts : PostsComponent.dataUpdatesubscribe()] '
        + 'before filter by name, fArtiL= ' + this.filteredArticles.length);
      this.filterByName();
      this.logger.debug('[posts.component.ts : PostsComponent.dataUpdatesubscribe()] '
        + 'after filter by name, fArtiL= ' + this.filteredArticles.length);

      this.filterByAuthor();
      this.logger.debug('[posts.component.ts : PostsComponent.dataUpdatesubscribe()] '
        + 'after filter by author, fArtiL= ' + this.filteredArticles.length);

    });

  }

  async ngOnInit() {
    // await this.getDataService.getAllPostsObservable().then((allPostInObservable: Post[]) => {
    //   this.articles = allPostInObservable;
    //   this.logger.debug('[posts.component.ts : PostsComponent.()] ' + 'this.articles.length' + this.articles.length);
    // });

    //// console.log(this.articles);
    // this.filteredArticles = this.articles;

    // this.filterByName();
    // this.filterByAuthor();
    this.logger.debug('[posts.component.ts : PostsComponent.ngOnInit()] ' + 'in ngOnInit');
    // this.logger.debug('[posts.component.ts : PostsComponent.ngOnInit()] ' + 'this.articles= ' + this.articles.length);

  }

  private filter(){
    this.filteredArticles = JSON.parse(JSON.stringify(this.articles));
    this.logger.debug('[posts.component.ts : PostsComponent.dataUpdatesubscribe()] '
      + 'before filter by name, fArtiL= ' + this.filteredArticles.length);
    this.filterByName();
    this.logger.debug('[posts.component.ts : PostsComponent.dataUpdatesubscribe()] '
      + 'after filter by name, fArtiL= ' + this.filteredArticles.length);

    this.filterByAuthor();
    this.logger.debug('[posts.component.ts : PostsComponent.dataUpdatesubscribe()] '
      + 'after filter by author, fArtiL= ' + this.filteredArticles.length);
  }
  private filterByName() {
    //// console.log(this.articles);
    if (this.searchText !== '' && this.articles !== undefined && this.searchText !== undefined) {
      this.logger.debug('[posts.component.ts : PostsComponent.filterByName()] ' + 'trying to filter by name with searchText ' + this.searchText);

      this.filteredArticles = [];
      this.articles.forEach(element => {

        const isInAuthor = element.author.toUpperCase().indexOf(this.searchText.toUpperCase()) >= 0;

        const isInText = element.text.toUpperCase().indexOf(this.searchText.toUpperCase()) >= 0;
        if (isInAuthor || isInText) {
          this.filteredArticles.push(element);
        }
      });
    } else {
      // this.filteredArticles = JSON.parse(JSON.stringify(this.articles));
      this.logger.debug('[posts.component.ts : PostsComponent.filterByName()] ' + 'not need to filter, filteredArticles.length ' + this.filteredArticles.length);

      // this.filteredArticles = this.articles;
    }
    //// console.log(this.filteredArticles);
    // this.ngOnInit();
    // window.location.reload();

  }

  private filterByAuthor() {
    this.logger.debug('[posts.component.ts : PostsComponent.filterByAuthor()] ');

    let i = 0;

    while (i < this.filteredArticles.length) {
      const isInAuthor = this.compareAuthArray(this.filteredArticles[i]) || (this.filteredArticles[i].author === this.loginUser.username);
      if (!isInAuthor) {
        this.filteredArticles.splice(i, 1);
      } else {
        i += 1;
      }
    }

  }

  private compareAuthArray(post: Post) {
    this.logger.debug('[posts.component.ts : PostsComponent.compareAuthArray()] ' + 'IdList' + this.followedUserIdList);
    // this.logger.debug('[posts.component.ts : PostsComponent.compareAuthArray()] ' + 'allUser' + this.allUsers);

    let isInAuthor = false;
    this.followedUserIdList.forEach(id => {
      let username = '';
      this.logger.debug('[posts.component.ts : PostsComponent.()] ' + 'this.allUsers: ' + this.allUsers);

      this.allUsers.forEach(user => {
        if (user.id === id) {
          username = user.username;
        }
      });
      this.logger.debug('[posts.component.ts : PostsComponent.()] ' + 'username' + username);

      if (post.author === username) {
        isInAuthor = true;
        return true;
      }

    });
    return isInAuthor;

  }
}
