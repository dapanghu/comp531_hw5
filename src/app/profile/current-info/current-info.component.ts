import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../user';
import {JsonDataService} from '../../json-data.service';

@Component({
  selector: 'app-current-info',
  templateUrl: './current-info.component.html',
  styleUrls: ['./current-info.component.css']
})
export class CurrentInfoComponent implements OnInit {
  @Input() loginUser: User = new User();

  constructor(private getDataService: JsonDataService) {

   // console.log('in current-info.component loginUser= ' + this.loginUser);
  }


  ngOnInit() {
    // this.loginUser = this.getDataService.getLoginUser();

    // this.loginUser = localStorage['loginUser'];

    //// console.log(localStorage['testInfo']);
    //// console.log('in profile current-info ngOnInit ');
    //// console.log(this.loginUser.username);

  }

}
