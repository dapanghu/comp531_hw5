import {async, ComponentFixture, fakeAsync, inject, TestBed, tick} from '@angular/core/testing';

import {BrowserModule, By} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {DebugElement} from '@angular/core';
import {LoginComponent} from '../auth/login/login.component';
import {FormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {JsonDataService} from '../json-data.service';

import {NGXLogger} from 'ngx-logger';
import {LoggerModule, NgxLoggerLevel} from 'ngx-logger';
import {AppRoutingModule} from '../app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule, MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule, MatSidenavModule,
  MatToolbarModule
} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {AppComponent} from '../app.component';
import {AuthComponent} from '../auth/auth.component';
import {RegisterationComponent} from '../auth/registeration/registeration.component';

import {ProfileComponent} from '../profile/profile.component';
import {CurrentInfoComponent} from '../profile/current-info/current-info.component';
import {UpdateInfoComponent} from '../profile/update-info/update-info.component';
import {APP_BASE_HREF, Location} from '@angular/common';
import {routes} from '../app-routing.module';
import {MainComponent} from '../main/main.component';
import {FollowingComponent} from '../main/following/following.component';
import {HeadlineComponent} from '../main/headline/headline.component';
import {PostsComponent} from '../main/posts/posts.component';

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MainComponent,
        FollowingComponent,
        AppComponent,
        AuthComponent,
        LoginComponent,
        RegisterationComponent,
        HeadlineComponent,
        PostsComponent,
        ProfileComponent,
        CurrentInfoComponent,
        UpdateInfoComponent],

      imports: [
        RouterTestingModule.withRoutes(routes),
        FormsModule,
        RouterTestingModule,
        FormsModule,
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatMenuModule,
        MatButtonModule,
        MatIconModule,
        MatCardModule,
        MatToolbarModule,
        MatFormFieldModule,
        MatInputModule,
        MatSidenavModule,
        FlexLayoutModule,
        MatGridListModule,
        HttpClientModule, LoggerModule.forRoot({
          serverLoggingUrl: '/api/logs',
          level: NgxLoggerLevel.DEBUG
        })
      ],
      providers: [JsonDataService, NGXLogger, {provide: APP_BASE_HREF, useValue: '/'}]

    })
    .compileComponents();
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should fetch the logged in user\'s profile information', () => {
    fixture.detectChanges();
    const currentUsername = fixture.debugElement.queryAll(By.css('.current-username'));
    expect(currentUsername[0].nativeElement.textContent.trim()).toEqual('Username: zx17');

    const currentEmail = fixture.debugElement.queryAll(By.css('.current-email'));
    expect(currentEmail[0].nativeElement.textContent.trim()).toEqual('Email: a@b.com');

    const currentPhone = fixture.debugElement.queryAll(By.css('.current-phone'));
    expect(currentPhone[0].nativeElement.textContent.trim()).toEqual('Phone: 111-111-1111');
  });
});
