import {Component, OnInit} from '@angular/core';
import {User} from '../user';
import {Router} from '@angular/router';
import {JsonDataService} from '../json-data.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  loginUser: User = new User();

  constructor(private router: Router, private getDataService: JsonDataService) {
    this.getDataService.getLoginPromise().subscribe((jsonUser: User) => {
      this.loginUser = jsonUser;
    });
  }

  ngOnInit() {
  }

}
