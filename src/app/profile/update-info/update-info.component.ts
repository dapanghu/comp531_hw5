import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../user';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {JsonDataService} from '../../json-data.service';

@Component({
  selector: 'app-update-info',
  templateUrl: './update-info.component.html',
  styleUrls: ['./update-info.component.css']
})
export class UpdateInfoComponent implements OnInit {

  @Input() loginUser: User = new User();

  constructor(private router: Router, private getDataService: JsonDataService) {
   // console.log('in update-info.component loginUser= ' + this.loginUser);
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    // console.log('form.value= ' + form.value);
    // console.log('form.valid= ' + form.valid);

    if (form.valid) {

      this.loginUser.username = form.value['username'];
      this.loginUser.email = form.value['email'];
      this.loginUser.phone = form.value['phone'];
      this.loginUser.zipcode = form.value['zipcode'];
      this.loginUser.password = form.value['password'];
     // console.log('form.value[\'username\']= ' + form.value['username']);

      // this.router.navigate(['main']);
      //// console.log('get input user title: ' + this.currentUser.username);
      //// console.log('get input user password: ' + this.currentUser.password);
    }
  }

}
