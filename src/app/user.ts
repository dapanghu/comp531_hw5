export class User {
  id: number;
  username: string;
  password: string;
  passwordConfirmation: string;
  zipcode: String;
  birthday: Date;
  phone: String;
  email: string;
  status: string;
  image: string;
  constructor() {
    this.username = '';
    this.password = '';
    this.passwordConfirmation = '';
    this.zipcode = '';
    this.birthday = new Date();
    this.phone = '';
    this.email = '';
  }
}


